-- *********************************************************************
-- Update Database Script
-- *********************************************************************
-- Change Log: xml
-- Ran at: 1/8/20 2:54 PM
-- Against: System@offline:h2
-- Liquibase version: 3.5.0
-- *********************************************************************

-- Changeset xml::Revision #0.0.1::Modeling Workbench
CREATE TABLE PUBLIC.ppc_bankproduct (code_ VARCHAR(3), associated_apps_ VARCHAR(100), account_prefix_ VARCHAR(150), account_format_ VARCHAR(100), interfaces_ VARCHAR(150), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, createdon_ TIMESTAMP NOT NULL, modifiedon_ TIMESTAMP NOT NULL, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_c55f47e7 PRIMARY KEY (id_));

CREATE INDEX PUBLIC.ppc_f82dd48 ON PUBLIC.ppc_bankproduct(code_);

CREATE INDEX PUBLIC.ppc_8f538b06 ON PUBLIC.ppc_bankproduct(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_8f594eeb ON PUBLIC.ppc_bankproduct(name_);

CREATE INDEX PUBLIC.ppc_49fd843 ON PUBLIC.ppc_bankproduct(revision_);

CREATE TABLE PUBLIC.ppc_bankproduct_l (attributename_ VARCHAR(64), orderindex_ NUMBER(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_eabcc99a PRIMARY KEY (id_));

COMMENT ON TABLE PUBLIC.ppc_bankproduct_l IS 'Association table for BankProduct';

CREATE INDEX PUBLIC.ppc_9be9b14 ON PUBLIC.ppc_bankproduct_l(attributename_);

CREATE INDEX PUBLIC.ppc_cdce3d77 ON PUBLIC.ppc_bankproduct_l(orderindex_);

CREATE INDEX PUBLIC.ppc_9bec9d6c ON PUBLIC.ppc_bankproduct_l(referencemodelid_);

CREATE INDEX PUBLIC.ppc_53404f4b ON PUBLIC.ppc_bankproduct_l(referencemodeltype_);

CREATE INDEX PUBLIC.ppc_8d353961 ON PUBLIC.ppc_bankproduct_l(referencedbymodelid_);

CREATE INDEX PUBLIC.ppc_14d1c200 ON PUBLIC.ppc_bankproduct_l(referencedbymodeltype_);

CREATE INDEX PUBLIC.ppc_bea9c39 ON PUBLIC.ppc_bankproduct_l(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_bf0601e ON PUBLIC.ppc_bankproduct_l(name_);

CREATE INDEX PUBLIC.ppc_d71854b0 ON PUBLIC.ppc_bankproduct_l(revision_);

CREATE TABLE PUBLIC.ppc_banksubproduct (code_ VARCHAR(2), market_type_ VARCHAR(4), iban_retl_comm_ind_ VARCHAR(1), iban_stmt_format_ VARCHAR(3), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, createdon_ TIMESTAMP NOT NULL, modifiedon_ TIMESTAMP NOT NULL, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_2bc165ef PRIMARY KEY (id_));

CREATE INDEX PUBLIC.ppc_8c242116 ON PUBLIC.ppc_banksubproduct(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_8c29e4fb ON PUBLIC.ppc_banksubproduct(name_);

CREATE INDEX PUBLIC.ppc_5f5c4033 ON PUBLIC.ppc_banksubproduct(revision_);

CREATE TABLE PUBLIC.ppc_banksubproduct_l (attributename_ VARCHAR(64), orderindex_ NUMBER(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_410f85a2 PRIMARY KEY (id_));

COMMENT ON TABLE PUBLIC.ppc_banksubproduct_l IS 'Association table for BankSubProduct';

CREATE INDEX PUBLIC.ppc_7cee9b04 ON PUBLIC.ppc_banksubproduct_l(attributename_);

CREATE INDEX PUBLIC.ppc_57efc387 ON PUBLIC.ppc_banksubproduct_l(orderindex_);

CREATE INDEX PUBLIC.ppc_1ab5577c ON PUBLIC.ppc_banksubproduct_l(referencemodelid_);

CREATE INDEX PUBLIC.ppc_42c2c55b ON PUBLIC.ppc_banksubproduct_l(referencemodeltype_);

CREATE INDEX PUBLIC.ppc_8e018551 ON PUBLIC.ppc_banksubproduct_l(referencedbymodelid_);

CREATE INDEX PUBLIC.ppc_13bad1f0 ON PUBLIC.ppc_banksubproduct_l(referencedbymodeltype_);

CREATE INDEX PUBLIC.ppc_16edee49 ON PUBLIC.ppc_banksubproduct_l(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_16f3b22e ON PUBLIC.ppc_banksubproduct_l(name_);

CREATE INDEX PUBLIC.ppc_745a80a0 ON PUBLIC.ppc_banksubproduct_l(revision_);

CREATE TABLE PUBLIC.ppc_bankingchannel (code_ VARCHAR(16), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, createdon_ TIMESTAMP NOT NULL, modifiedon_ TIMESTAMP NOT NULL, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_b1ac847d PRIMARY KEY (id_));

CREATE INDEX PUBLIC.ppc_25676c66 ON PUBLIC.ppc_bankingchannel(code_);

CREATE INDEX PUBLIC.ppc_d32148a4 ON PUBLIC.ppc_bankingchannel(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_d3270c89 ON PUBLIC.ppc_bankingchannel(name_);

CREATE INDEX PUBLIC.ppc_9b74eee5 ON PUBLIC.ppc_bankingchannel(revision_);

CREATE TABLE PUBLIC.ppc_bankingchannel_l (attributename_ VARCHAR(64), orderindex_ NUMBER(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_f8ad38b0 PRIMARY KEY (id_));

COMMENT ON TABLE PUBLIC.ppc_bankingchannel_l IS 'Association table for BankingChannel';

CREATE INDEX PUBLIC.ppc_3a8cceb6 ON PUBLIC.ppc_bankingchannel_l(attributename_);

CREATE INDEX PUBLIC.ppc_fccb7d15 ON PUBLIC.ppc_bankingchannel_l(orderindex_);

CREATE INDEX PUBLIC.ppc_27d72e8a ON PUBLIC.ppc_bankingchannel_l(referencemodelid_);

CREATE INDEX PUBLIC.ppc_8ecb10e9 ON PUBLIC.ppc_bankingchannel_l(referencemodeltype_);

CREATE INDEX PUBLIC.ppc_c302ab83 ON PUBLIC.ppc_bankingchannel_l(referencedbymodelid_);

CREATE INDEX PUBLIC.ppc_d0b33a2 ON PUBLIC.ppc_bankingchannel_l(referencedbymodeltype_);

CREATE INDEX PUBLIC.ppc_933f6a57 ON PUBLIC.ppc_bankingchannel_l(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_93452e3c ON PUBLIC.ppc_bankingchannel_l(name_);

CREATE INDEX PUBLIC.ppc_d024ad2 ON PUBLIC.ppc_bankingchannel_l(revision_);

CREATE TABLE PUBLIC.ppc_bankinglineofbusiness (code_ VARCHAR(16), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, createdon_ TIMESTAMP NOT NULL, modifiedon_ TIMESTAMP NOT NULL, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_6bd38f29 PRIMARY KEY (id_));

CREATE INDEX PUBLIC.ppc_64c5f0a ON PUBLIC.ppc_bankinglineofbusiness(code_);

CREATE INDEX PUBLIC.ppc_b4ca0648 ON PUBLIC.ppc_bankinglineofbusiness(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_b4cfca2d ON PUBLIC.ppc_bankinglineofbusiness(name_);

CREATE INDEX PUBLIC.ppc_60abecc1 ON PUBLIC.ppc_bankinglineofbusiness(revision_);

CREATE TABLE PUBLIC.ppc_bankinglineofbiz_l (attributename_ VARCHAR(64), orderindex_ NUMBER(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_2c402eeb PRIMARY KEY (id_));

COMMENT ON TABLE PUBLIC.ppc_bankinglineofbiz_l IS 'Association table for BankingLineOfBusiness';

CREATE INDEX PUBLIC.ppc_a1b0f15b ON PUBLIC.ppc_bankinglineofbiz_l(attributename_);

CREATE INDEX PUBLIC.ppc_4af1e490 ON PUBLIC.ppc_bankinglineofbiz_l(orderindex_);

CREATE INDEX PUBLIC.ppc_cdf2cdc5 ON PUBLIC.ppc_bankinglineofbiz_l(referencemodelid_);

CREATE INDEX PUBLIC.ppc_1c7bcd64 ON PUBLIC.ppc_bankinglineofbiz_l(referencemodeltype_);

CREATE INDEX PUBLIC.ppc_eb697e68 ON PUBLIC.ppc_bankinglineofbiz_l(referencedbymodelid_);

CREATE INDEX PUBLIC.ppc_b708e147 ON PUBLIC.ppc_bankinglineofbiz_l(referencedbymodeltype_);

CREATE INDEX PUBLIC.ppc_fa3bca12 ON PUBLIC.ppc_bankinglineofbiz_l(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_fa418df7 ON PUBLIC.ppc_bankinglineofbiz_l(name_);

CREATE INDEX PUBLIC.ppc_736d87b7 ON PUBLIC.ppc_bankinglineofbiz_l(revision_);

CREATE TABLE PUBLIC.ppc_bankingregion (code_ VARCHAR(4), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, createdon_ TIMESTAMP NOT NULL, modifiedon_ TIMESTAMP NOT NULL, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_1e643320 PRIMARY KEY (id_));

CREATE INDEX PUBLIC.ppc_1e94b641 ON PUBLIC.ppc_bankingregion(code_);

CREATE INDEX PUBLIC.ppc_e315d2bf ON PUBLIC.ppc_bankingregion(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_e31b96a4 ON PUBLIC.ppc_bankingregion(name_);

CREATE INDEX PUBLIC.ppc_de09356a ON PUBLIC.ppc_bankingregion(revision_);

CREATE TABLE PUBLIC.ppc_bankingregion_l (attributename_ VARCHAR(64), orderindex_ NUMBER(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_1633ca93 PRIMARY KEY (id_));

COMMENT ON TABLE PUBLIC.ppc_bankingregion_l IS 'Association table for BankingRegion';

CREATE INDEX PUBLIC.ppc_fcfa75bb ON PUBLIC.ppc_bankingregion_l(attributename_);

CREATE INDEX PUBLIC.ppc_5fcdbc30 ON PUBLIC.ppc_bankingregion_l(orderindex_);

CREATE INDEX PUBLIC.ppc_fe366d65 ON PUBLIC.ppc_bankingregion_l(referencemodelid_);

CREATE INDEX PUBLIC.ppc_4a560504 ON PUBLIC.ppc_bankingregion_l(referencemodeltype_);

CREATE INDEX PUBLIC.ppc_78d63ac8 ON PUBLIC.ppc_bankingregion_l(referencedbymodelid_);

CREATE INDEX PUBLIC.ppc_9c3805a7 ON PUBLIC.ppc_bankingregion_l(referencedbymodeltype_);

CREATE INDEX PUBLIC.ppc_7839d9b2 ON PUBLIC.ppc_bankingregion_l(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_783f9d97 ON PUBLIC.ppc_bankingregion_l(name_);

CREATE INDEX PUBLIC.ppc_fb9f0417 ON PUBLIC.ppc_bankingregion_l(revision_);

CREATE TABLE PUBLIC.ppc_disclosurefee (code_ VARCHAR(16), fieldtype_ VARCHAR(256), is_regionalized_ NUMBER(1) DEFAULT 1, id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, createdon_ TIMESTAMP NOT NULL, modifiedon_ TIMESTAMP NOT NULL, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_58cb7ee1 PRIMARY KEY (id_));

CREATE INDEX PUBLIC.ppc_50504c2 ON PUBLIC.ppc_disclosurefee(code_);

CREATE INDEX PUBLIC.ppc_bd7f5600 ON PUBLIC.ppc_disclosurefee(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_bd8519e5 ON PUBLIC.ppc_disclosurefee(name_);

CREATE INDEX PUBLIC.ppc_58b1c609 ON PUBLIC.ppc_disclosurefee(revision_);

CREATE TABLE PUBLIC.ppc_disclosurefee_l (attributename_ VARCHAR(64), orderindex_ NUMBER(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_53f72a14 PRIMARY KEY (id_));

COMMENT ON TABLE PUBLIC.ppc_disclosurefee_l IS 'Association table for DisclosureFee';

CREATE INDEX PUBLIC.ppc_92ac0fda ON PUBLIC.ppc_disclosurefee_l(attributename_);

CREATE INDEX PUBLIC.ppc_6b646e71 ON PUBLIC.ppc_disclosurefee_l(orderindex_);

CREATE INDEX PUBLIC.ppc_4f4aae6 ON PUBLIC.ppc_disclosurefee_l(referencemodelid_);

CREATE INDEX PUBLIC.ppc_9a7ae645 ON PUBLIC.ppc_disclosurefee_l(referencemodeltype_);

CREATE INDEX PUBLIC.ppc_2d4d81a7 ON PUBLIC.ppc_disclosurefee_l(referencedbymodelid_);

CREATE INDEX PUBLIC.ppc_ff910c6 ON PUBLIC.ppc_disclosurefee_l(referencedbymodeltype_);

CREATE INDEX PUBLIC.ppc_5e4f90b3 ON PUBLIC.ppc_disclosurefee_l(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_5e555498 ON PUBLIC.ppc_disclosurefee_l(name_);

CREATE INDEX PUBLIC.ppc_6e65e8f6 ON PUBLIC.ppc_disclosurefee_l(revision_);

CREATE TABLE PUBLIC.ppc_disclosurefeeperiod (id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, createdon_ TIMESTAMP NOT NULL, modifiedon_ TIMESTAMP NOT NULL, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_f74a8440 PRIMARY KEY (id_));

CREATE INDEX PUBLIC.ppc_d62c1126 ON PUBLIC.ppc_disclosurefeeperiod(branchid_, temporalmodelid_);

CREATE INDEX PUBLIC.ppc_d62bf6df ON PUBLIC.ppc_disclosurefeeperiod(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_d631bac4 ON PUBLIC.ppc_disclosurefeeperiod(name_);

CREATE INDEX PUBLIC.ppc_9b8e0d4a ON PUBLIC.ppc_disclosurefeeperiod(revision_);

CREATE TABLE PUBLIC.ppc_disclosurefeeperiod_l (attributename_ VARCHAR(64), orderindex_ NUMBER(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_4eca53b3 PRIMARY KEY (id_));

COMMENT ON TABLE PUBLIC.ppc_disclosurefeeperiod_l IS 'Association table for DisclosureFeePeriod';

CREATE INDEX PUBLIC.ppc_519a7d9b ON PUBLIC.ppc_disclosurefeeperiod_l(attributename_);

CREATE INDEX PUBLIC.ppc_50dc050 ON PUBLIC.ppc_disclosurefeeperiod_l(orderindex_);

CREATE INDEX PUBLIC.ppc_e92ad985 ON PUBLIC.ppc_disclosurefeeperiod_l(referencemodelid_);

CREATE INDEX PUBLIC.ppc_49dfe924 ON PUBLIC.ppc_disclosurefeeperiod_l(referencemodeltype_);

CREATE INDEX PUBLIC.ppc_6a88daa8 ON PUBLIC.ppc_disclosurefeeperiod_l(referencedbymodelid_);

CREATE INDEX PUBLIC.ppc_ebc22d87 ON PUBLIC.ppc_disclosurefeeperiod_l(referencedbymodeltype_);

CREATE INDEX PUBLIC.ppc_fe5775d2 ON PUBLIC.ppc_disclosurefeeperiod_l(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_fe5d39b7 ON PUBLIC.ppc_disclosurefeeperiod_l(name_);

CREATE INDEX PUBLIC.ppc_6b4d63f7 ON PUBLIC.ppc_disclosurefeeperiod_l(revision_);

CREATE TABLE PUBLIC.ppc_disclosurefeeplan (code_ VARCHAR(16), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, createdon_ TIMESTAMP NOT NULL, modifiedon_ TIMESTAMP NOT NULL, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_b64bdb98 PRIMARY KEY (id_));

CREATE INDEX PUBLIC.ppc_c0c72ab9 ON PUBLIC.ppc_disclosurefeeplan(code_);

CREATE INDEX PUBLIC.ppc_e7770137 ON PUBLIC.ppc_disclosurefeeplan(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_e77cc51c ON PUBLIC.ppc_disclosurefeeplan(name_);

CREATE INDEX PUBLIC.ppc_5a0c57f2 ON PUBLIC.ppc_disclosurefeeplan(revision_);

CREATE TABLE PUBLIC.ppc_disclosurefeeplan_l (attributename_ VARCHAR(64), orderindex_ NUMBER(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_52d3350b PRIMARY KEY (id_));

COMMENT ON TABLE PUBLIC.ppc_disclosurefeeplan_l IS 'Association table for DisclosureFeePlan';

CREATE INDEX PUBLIC.ppc_a8f60c43 ON PUBLIC.ppc_disclosurefeeplan_l(attributename_);

CREATE INDEX PUBLIC.ppc_3f34f2a8 ON PUBLIC.ppc_disclosurefeeplan_l(orderindex_);

CREATE INDEX PUBLIC.ppc_d0c8e9dd ON PUBLIC.ppc_disclosurefeeplan_l(referencemodelid_);

CREATE INDEX PUBLIC.ppc_c23b437c ON PUBLIC.ppc_disclosurefeeplan_l(referencemodeltype_);

CREATE INDEX PUBLIC.ppc_fd98cb50 ON PUBLIC.ppc_disclosurefeeplan_l(referencedbymodelid_);

CREATE INDEX PUBLIC.ppc_fa98942f ON PUBLIC.ppc_disclosurefeeplan_l(referencedbymodeltype_);

CREATE INDEX PUBLIC.ppc_e9094a2a ON PUBLIC.ppc_disclosurefeeplan_l(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_e90f0e0f ON PUBLIC.ppc_disclosurefeeplan_l(name_);

CREATE INDEX PUBLIC.ppc_8363a49f ON PUBLIC.ppc_disclosurefeeplan_l(revision_);

CREATE TABLE PUBLIC.ppc_disclosureform (code_ VARCHAR(16), printing_option_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, createdon_ TIMESTAMP NOT NULL, modifiedon_ TIMESTAMP NOT NULL, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_d272b1e9 PRIMARY KEY (id_));

CREATE INDEX PUBLIC.ppc_8a94c2d2 ON PUBLIC.ppc_disclosureform(code_);

CREATE INDEX PUBLIC.ppc_a0fae010 ON PUBLIC.ppc_disclosureform(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_a100a3f5 ON PUBLIC.ppc_disclosureform(name_);

CREATE INDEX PUBLIC.ppc_9194b9f9 ON PUBLIC.ppc_disclosureform(revision_);

CREATE TABLE PUBLIC.ppc_disclosureform_l (attributename_ VARCHAR(64), orderindex_ NUMBER(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_9dbb1c PRIMARY KEY (id_));

COMMENT ON TABLE PUBLIC.ppc_disclosureform_l IS 'Association table for DisclosureForm';

CREATE INDEX PUBLIC.ppc_82909bca ON PUBLIC.ppc_disclosureform_l(attributename_);

CREATE INDEX PUBLIC.ppc_cbfce881 ON PUBLIC.ppc_disclosureform_l(orderindex_);

CREATE INDEX PUBLIC.ppc_9a2d58f6 ON PUBLIC.ppc_disclosureform_l(referencemodelid_);

CREATE INDEX PUBLIC.ppc_c4405055 ON PUBLIC.ppc_disclosureform_l(referencemodeltype_);

CREATE INDEX PUBLIC.ppc_3c355997 ON PUBLIC.ppc_disclosureform_l(referencedbymodelid_);

CREATE INDEX PUBLIC.ppc_44aacb6 ON PUBLIC.ppc_disclosureform_l(referencedbymodeltype_);

CREATE INDEX PUBLIC.ppc_5110d6c3 ON PUBLIC.ppc_disclosureform_l(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_51169aa8 ON PUBLIC.ppc_disclosureform_l(name_);

CREATE INDEX PUBLIC.ppc_fa5ba0e6 ON PUBLIC.ppc_disclosureform_l(revision_);

CREATE TABLE PUBLIC.ppc_disclosureformperiod (id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, createdon_ TIMESTAMP NOT NULL, modifiedon_ TIMESTAMP NOT NULL, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_dcb15148 PRIMARY KEY (id_));

CREATE INDEX PUBLIC.ppc_5bbbcf36 ON PUBLIC.ppc_disclosureformperiod(branchid_, temporalmodelid_);

CREATE INDEX PUBLIC.ppc_5bbbb4ef ON PUBLIC.ppc_disclosureformperiod(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_5bc178d4 ON PUBLIC.ppc_disclosureformperiod(name_);

CREATE INDEX PUBLIC.ppc_45064d3a ON PUBLIC.ppc_disclosureformperiod(revision_);

CREATE TABLE PUBLIC.ppc_disclosureformperiod_l (attributename_ VARCHAR(64), orderindex_ NUMBER(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_75b1febb PRIMARY KEY (id_));

COMMENT ON TABLE PUBLIC.ppc_disclosureformperiod_l IS 'Association table for DisclosureFormPeriod';

CREATE INDEX PUBLIC.ppc_6082558b ON PUBLIC.ppc_disclosureformperiod_l(attributename_);

CREATE INDEX PUBLIC.ppc_9a466e60 ON PUBLIC.ppc_disclosureformperiod_l(orderindex_);

CREATE INDEX PUBLIC.ppc_7f0cbb95 ON PUBLIC.ppc_disclosureformperiod_l(referencemodelid_);

CREATE INDEX PUBLIC.ppc_eed18734 ON PUBLIC.ppc_disclosureformperiod_l(referencemodeltype_);

CREATE INDEX PUBLIC.ppc_63cafe98 ON PUBLIC.ppc_disclosureformperiod_l(referencedbymodelid_);

CREATE INDEX PUBLIC.ppc_9d0b1577 ON PUBLIC.ppc_disclosureformperiod_l(referencedbymodeltype_);

CREATE INDEX PUBLIC.ppc_5eefefe2 ON PUBLIC.ppc_disclosureformperiod_l(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_5ef5b3c7 ON PUBLIC.ppc_disclosureformperiod_l(name_);

CREATE INDEX PUBLIC.ppc_97b567e7 ON PUBLIC.ppc_disclosureformperiod_l(revision_);

CREATE TABLE PUBLIC.ppc_disclosureformplan (code_ VARCHAR(16), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, createdon_ TIMESTAMP NOT NULL, modifiedon_ TIMESTAMP NOT NULL, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_d3924aa0 PRIMARY KEY (id_));

CREATE INDEX PUBLIC.ppc_5d2960c9 ON PUBLIC.ppc_disclosureformplan(code_);

CREATE INDEX PUBLIC.ppc_2eff0347 ON PUBLIC.ppc_disclosureformplan(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_2f04c72c ON PUBLIC.ppc_disclosureformplan(name_);

CREATE INDEX PUBLIC.ppc_bf73d3e2 ON PUBLIC.ppc_disclosureformplan(revision_);

CREATE TABLE PUBLIC.ppc_disclosureformplan_l (attributename_ VARCHAR(64), orderindex_ NUMBER(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_383a0213 PRIMARY KEY (id_));

COMMENT ON TABLE PUBLIC.ppc_disclosureformplan_l IS 'Association table for DisclosureFormPlan';

CREATE INDEX PUBLIC.ppc_bad32033 ON PUBLIC.ppc_disclosureformplan_l(attributename_);

CREATE INDEX PUBLIC.ppc_4be1e4b8 ON PUBLIC.ppc_disclosureformplan_l(orderindex_);

CREATE INDEX PUBLIC.ppc_9edc0fed ON PUBLIC.ppc_disclosureformplan_l(referencemodelid_);

CREATE INDEX PUBLIC.ppc_581d258c ON PUBLIC.ppc_disclosureformplan_l(referencemodeltype_);

CREATE INDEX PUBLIC.ppc_23f32b40 ON PUBLIC.ppc_disclosureformplan_l(referencedbymodelid_);

CREATE INDEX PUBLIC.ppc_f3dab81f ON PUBLIC.ppc_disclosureformplan_l(referencedbymodeltype_);

CREATE INDEX PUBLIC.ppc_6e99083a ON PUBLIC.ppc_disclosureformplan_l(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_6e9ecc1f ON PUBLIC.ppc_disclosureformplan_l(name_);

CREATE INDEX PUBLIC.ppc_2cdbe48f ON PUBLIC.ppc_disclosureformplan_l(revision_);

CREATE TABLE PUBLIC.ppc_disclosurewaiver (code_ VARCHAR(16), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, createdon_ TIMESTAMP NOT NULL, modifiedon_ TIMESTAMP NOT NULL, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_5e737589 PRIMARY KEY (id_));

CREATE INDEX PUBLIC.ppc_b7aafd72 ON PUBLIC.ppc_disclosurewaiver(code_);

CREATE INDEX PUBLIC.ppc_ff7992b0 ON PUBLIC.ppc_disclosurewaiver(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_ff7f5695 ON PUBLIC.ppc_disclosurewaiver(name_);

CREATE INDEX PUBLIC.ppc_a5253359 ON PUBLIC.ppc_disclosurewaiver(revision_);

CREATE TABLE PUBLIC.ppc_disclosurewaiver_l (attributename_ VARCHAR(64), orderindex_ NUMBER(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_8f7c16bc PRIMARY KEY (id_));

COMMENT ON TABLE PUBLIC.ppc_disclosurewaiver_l IS 'Association table for DisclosureWaiver';

CREATE INDEX PUBLIC.ppc_1d56052a ON PUBLIC.ppc_disclosurewaiver_l(attributename_);

CREATE INDEX PUBLIC.ppc_c6efb21 ON PUBLIC.ppc_disclosurewaiver_l(orderindex_);

CREATE INDEX PUBLIC.ppc_7d2ef396 ON PUBLIC.ppc_disclosurewaiver_l(referencemodelid_);

CREATE INDEX PUBLIC.ppc_ed45c2f5 ON PUBLIC.ppc_disclosurewaiver_l(referencemodeltype_);

CREATE INDEX PUBLIC.ppc_33de3af7 ON PUBLIC.ppc_disclosurewaiver_l(referencedbymodelid_);

CREATE INDEX PUBLIC.ppc_b540b616 ON PUBLIC.ppc_disclosurewaiver_l(referencedbymodeltype_);

CREATE INDEX PUBLIC.ppc_aad6163 ON PUBLIC.ppc_disclosurewaiver_l(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_ab32548 ON PUBLIC.ppc_disclosurewaiver_l(name_);

CREATE INDEX PUBLIC.ppc_6bb34246 ON PUBLIC.ppc_disclosurewaiver_l(revision_);

CREATE TABLE PUBLIC.ppc_featuresandservices (code_ VARCHAR(16), fieldtype_ VARCHAR(256), is_regionalized_ NUMBER(1) DEFAULT 1, id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, createdon_ TIMESTAMP NOT NULL, modifiedon_ TIMESTAMP NOT NULL, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_a1f00e22 PRIMARY KEY (id_));

CREATE INDEX PUBLIC.ppc_1c3b59c3 ON PUBLIC.ppc_featuresandservices(code_);

CREATE INDEX PUBLIC.ppc_3d55bfc1 ON PUBLIC.ppc_featuresandservices(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_3d5b83a6 ON PUBLIC.ppc_featuresandservices(name_);

CREATE INDEX PUBLIC.ppc_f176a028 ON PUBLIC.ppc_featuresandservices(revision_);

CREATE TABLE PUBLIC.ppc_featuresandservices_l (attributename_ VARCHAR(64), orderindex_ NUMBER(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_e634ed15 PRIMARY KEY (id_));

COMMENT ON TABLE PUBLIC.ppc_featuresandservices_l IS 'Association table for FeaturesAndServices';

CREATE INDEX PUBLIC.ppc_9ad7b379 ON PUBLIC.ppc_featuresandservices_l(attributename_);

CREATE INDEX PUBLIC.ppc_8e598732 ON PUBLIC.ppc_featuresandservices_l(orderindex_);

CREATE INDEX PUBLIC.ppc_d34a6ee7 ON PUBLIC.ppc_featuresandservices_l(referencemodelid_);

CREATE INDEX PUBLIC.ppc_2a6fae06 ON PUBLIC.ppc_featuresandservices_l(referencemodeltype_);

CREATE INDEX PUBLIC.ppc_9bf1b206 ON PUBLIC.ppc_featuresandservices_l(referencedbymodelid_);

CREATE INDEX PUBLIC.ppc_6652a565 ON PUBLIC.ppc_featuresandservices_l(referencedbymodeltype_);

CREATE INDEX PUBLIC.ppc_42328e34 ON PUBLIC.ppc_featuresandservices_l(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_42385219 ON PUBLIC.ppc_featuresandservices_l(name_);

CREATE INDEX PUBLIC.ppc_e95cb755 ON PUBLIC.ppc_featuresandservices_l(revision_);

CREATE TABLE PUBLIC.ppc_featuresandservicesplan (code_ VARCHAR(16), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, createdon_ TIMESTAMP NOT NULL, modifiedon_ TIMESTAMP NOT NULL, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_b024e259 PRIMARY KEY (id_));

CREATE INDEX PUBLIC.ppc_fad9173a ON PUBLIC.ppc_featuresandservicesplan(code_);

CREATE INDEX PUBLIC.ppc_5e90a278 ON PUBLIC.ppc_featuresandservicesplan(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_5e96665d ON PUBLIC.ppc_featuresandservicesplan(name_);

CREATE INDEX PUBLIC.ppc_f2890a91 ON PUBLIC.ppc_featuresandservicesplan(revision_);

CREATE TABLE PUBLIC.ppc_featuresandsrvcspln_l (attributename_ VARCHAR(64), orderindex_ NUMBER(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_f738b9a8 PRIMARY KEY (id_));

COMMENT ON TABLE PUBLIC.ppc_featuresandsrvcspln_l IS 'Association table for FeaturesAndServicesPlan';

CREATE INDEX PUBLIC.ppc_f5ba2dc6 ON PUBLIC.ppc_featuresandsrvcspln_l(attributename_);

CREATE INDEX PUBLIC.ppc_3560f805 ON PUBLIC.ppc_featuresandsrvcspln_l(orderindex_);

CREATE INDEX PUBLIC.ppc_2cc0b57a ON PUBLIC.ppc_featuresandsrvcspln_l(referencemodelid_);

CREATE INDEX PUBLIC.ppc_ff6e9bd9 ON PUBLIC.ppc_featuresandsrvcspln_l(referencemodeltype_);

CREATE INDEX PUBLIC.ppc_66d07e93 ON PUBLIC.ppc_featuresandsrvcspln_l(referencedbymodelid_);

CREATE INDEX PUBLIC.ppc_f4b082b2 ON PUBLIC.ppc_featuresandsrvcspln_l(referencedbymodeltype_);

CREATE INDEX PUBLIC.ppc_5c87d947 ON PUBLIC.ppc_featuresandsrvcspln_l(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_5c8d9d2c ON PUBLIC.ppc_featuresandsrvcspln_l(name_);

CREATE INDEX PUBLIC.ppc_f2783de2 ON PUBLIC.ppc_featuresandsrvcspln_l(revision_);

CREATE TABLE PUBLIC.ppc_featuresandservicestmprl (id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, createdon_ TIMESTAMP NOT NULL, modifiedon_ TIMESTAMP NOT NULL, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_42b99f61 PRIMARY KEY (id_));

CREATE INDEX PUBLIC.ppc_fce4a6cf ON PUBLIC.ppc_featuresandservicestmprl(branchid_, temporalmodelid_);

CREATE INDEX PUBLIC.ppc_fce48c88 ON PUBLIC.ppc_featuresandservicestmprl(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_fcea506d ON PUBLIC.ppc_featuresandservicestmprl(name_);

CREATE INDEX PUBLIC.ppc_84079e81 ON PUBLIC.ppc_featuresandservicestmprl(revision_);

CREATE TABLE PUBLIC.ppc_featuresandsrvcstmprl_l (attributename_ VARCHAR(64), orderindex_ NUMBER(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_2e43c229 PRIMARY KEY (id_));

COMMENT ON TABLE PUBLIC.ppc_featuresandsrvcstmprl_l IS 'Association table for FeaturesAndServicesTemporal';

CREATE INDEX PUBLIC.ppc_c2097de5 ON PUBLIC.ppc_featuresandsrvcstmprl_l(attributename_);

CREATE INDEX PUBLIC.ppc_9825b446 ON PUBLIC.ppc_featuresandsrvcstmprl_l(orderindex_);

CREATE INDEX PUBLIC.ppc_ee7d7cfb ON PUBLIC.ppc_featuresandsrvcstmprl_l(referencemodelid_);

CREATE INDEX PUBLIC.ppc_4517871a ON PUBLIC.ppc_featuresandsrvcstmprl_l(referencemodeltype_);

CREATE INDEX PUBLIC.ppc_d644fb72 ON PUBLIC.ppc_featuresandsrvcstmprl_l(referencedbymodelid_);

CREATE INDEX PUBLIC.ppc_58f943d1 ON PUBLIC.ppc_featuresandsrvcstmprl_l(referencedbymodeltype_);

CREATE INDEX PUBLIC.ppc_ef9b1a48 ON PUBLIC.ppc_featuresandsrvcstmprl_l(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_efa0de2d ON PUBLIC.ppc_featuresandsrvcstmprl_l(name_);

CREATE INDEX PUBLIC.ppc_bd6858c1 ON PUBLIC.ppc_featuresandsrvcstmprl_l(revision_);

CREATE TABLE PUBLIC.ppc_offeringstatus (code_ VARCHAR(16), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, createdon_ TIMESTAMP NOT NULL, modifiedon_ TIMESTAMP NOT NULL, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_a7b98d42 PRIMARY KEY (id_));

CREATE INDEX PUBLIC.ppc_68c802eb ON PUBLIC.ppc_offeringstatus(code_);

CREATE INDEX PUBLIC.ppc_764266e9 ON PUBLIC.ppc_offeringstatus(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_76482ace ON PUBLIC.ppc_offeringstatus(name_);

CREATE INDEX PUBLIC.ppc_d2447400 ON PUBLIC.ppc_offeringstatus(revision_);

CREATE TABLE PUBLIC.ppc_offeringstatus_l (attributename_ VARCHAR(64), orderindex_ NUMBER(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_9f9b2435 PRIMARY KEY (id_));

COMMENT ON TABLE PUBLIC.ppc_offeringstatus_l IS 'Association table for OfferingStatus';

CREATE INDEX PUBLIC.ppc_ed0e8351 ON PUBLIC.ppc_offeringstatus_l(attributename_);

CREATE INDEX PUBLIC.ppc_ea60865a ON PUBLIC.ppc_offeringstatus_l(orderindex_);

CREATE INDEX PUBLIC.ppc_2bcf700f ON PUBLIC.ppc_offeringstatus_l(referencemodelid_);

CREATE INDEX PUBLIC.ppc_75b9052e ON PUBLIC.ppc_offeringstatus_l(referencemodeltype_);

CREATE INDEX PUBLIC.ppc_b9d33fde ON PUBLIC.ppc_offeringstatus_l(referencedbymodelid_);

CREATE INDEX PUBLIC.ppc_92081d3d ON PUBLIC.ppc_offeringstatus_l(referencedbymodeltype_);

CREATE INDEX PUBLIC.ppc_f2920b5c ON PUBLIC.ppc_offeringstatus_l(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_f297cf41 ON PUBLIC.ppc_offeringstatus_l(name_);

CREATE INDEX PUBLIC.ppc_ce04f52d ON PUBLIC.ppc_offeringstatus_l(revision_);

CREATE TABLE PUBLIC.ppc_ppcproduct (code_ VARCHAR(6), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, createdon_ TIMESTAMP NOT NULL, modifiedon_ TIMESTAMP NOT NULL, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_d4e3bf0e PRIMARY KEY (id_));

CREATE INDEX PUBLIC.ppc_ea0127b7 ON PUBLIC.ppc_ppcproduct(code_);

CREATE INDEX PUBLIC.ppc_f3a14b5 ON PUBLIC.ppc_ppcproduct(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_f3fd89a ON PUBLIC.ppc_ppcproduct(name_);

CREATE INDEX PUBLIC.ppc_eff9dcb4 ON PUBLIC.ppc_ppcproduct(revision_);

CREATE TABLE PUBLIC.ppc_ppcproduct_l (attributename_ VARCHAR(64), orderindex_ NUMBER(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_2b001301 PRIMARY KEY (id_));

COMMENT ON TABLE PUBLIC.ppc_ppcproduct_l IS 'Association table for PpcProduct';

CREATE INDEX PUBLIC.ppc_12087e05 ON PUBLIC.ppc_ppcproduct_l(attributename_);

CREATE INDEX PUBLIC.ppc_1e3a826 ON PUBLIC.ppc_ppcproduct_l(orderindex_);

CREATE INDEX PUBLIC.ppc_2a2d08db ON PUBLIC.ppc_ppcproduct_l(referencemodelid_);

CREATE INDEX PUBLIC.ppc_53139afa ON PUBLIC.ppc_ppcproduct_l(referencemodeltype_);

CREATE INDEX PUBLIC.ppc_87cb6392 ON PUBLIC.ppc_ppcproduct_l(referencedbymodelid_);

CREATE INDEX PUBLIC.ppc_c28623f1 ON PUBLIC.ppc_ppcproduct_l(referencedbymodeltype_);

CREATE INDEX PUBLIC.ppc_2c557628 ON PUBLIC.ppc_ppcproduct_l(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_2c5b3a0d ON PUBLIC.ppc_ppcproduct_l(name_);

CREATE INDEX PUBLIC.ppc_540300e1 ON PUBLIC.ppc_ppcproduct_l(revision_);

CREATE TABLE PUBLIC.ppc_prodavailability (id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, createdon_ TIMESTAMP NOT NULL, modifiedon_ TIMESTAMP NOT NULL, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_9d8b848 PRIMARY KEY (id_));

CREATE INDEX PUBLIC.ppc_1cf17836 ON PUBLIC.ppc_prodavailability(branchid_, temporalmodelid_);

CREATE INDEX PUBLIC.ppc_1cf15def ON PUBLIC.ppc_prodavailability(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_1cf721d4 ON PUBLIC.ppc_prodavailability(name_);

CREATE INDEX PUBLIC.ppc_21f7843a ON PUBLIC.ppc_prodavailability(revision_);

CREATE TABLE PUBLIC.ppc_prodavailability_l (attributename_ VARCHAR(64), orderindex_ NUMBER(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_f69ba5bb PRIMARY KEY (id_));

COMMENT ON TABLE PUBLIC.ppc_prodavailability_l IS 'Association table for ProdAvailability';

CREATE INDEX PUBLIC.ppc_82590c8b ON PUBLIC.ppc_prodavailability_l(attributename_);

CREATE INDEX PUBLIC.ppc_83c31760 ON PUBLIC.ppc_prodavailability_l(orderindex_);

CREATE INDEX PUBLIC.ppc_58a6a495 ON PUBLIC.ppc_prodavailability_l(referencemodelid_);

CREATE INDEX PUBLIC.ppc_c9953034 ON PUBLIC.ppc_prodavailability_l(referencemodeltype_);

CREATE INDEX PUBLIC.ppc_e17c7598 ON PUBLIC.ppc_prodavailability_l(referencedbymodelid_);

CREATE INDEX PUBLIC.ppc_743acc77 ON PUBLIC.ppc_prodavailability_l(referencedbymodeltype_);

CREATE INDEX PUBLIC.ppc_a95f58e2 ON PUBLIC.ppc_prodavailability_l(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_a9651cc7 ON PUBLIC.ppc_prodavailability_l(name_);

CREATE INDEX PUBLIC.ppc_fd34dee7 ON PUBLIC.ppc_prodavailability_l(revision_);

CREATE TABLE PUBLIC.ppc_proddisclosurefeatureplan (id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, createdon_ TIMESTAMP NOT NULL, modifiedon_ TIMESTAMP NOT NULL, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_8a94bdff PRIMARY KEY (id_));

CREATE INDEX PUBLIC.ppc_3f5c7665 ON PUBLIC.ppc_proddisclosurefeatureplan(branchid_, temporalmodelid_);

CREATE INDEX PUBLIC.ppc_3f5c5c1e ON PUBLIC.ppc_proddisclosurefeatureplan(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_3f622003 ON PUBLIC.ppc_proddisclosurefeatureplan(name_);

CREATE INDEX PUBLIC.ppc_862c842b ON PUBLIC.ppc_proddisclosurefeatureplan(revision_);

CREATE TABLE PUBLIC.ppc_proddisclosureftrpln_l (attributename_ VARCHAR(64), orderindex_ NUMBER(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_8f546c5b PRIMARY KEY (id_));

COMMENT ON TABLE PUBLIC.ppc_proddisclosureftrpln_l IS 'Association table for ProdDisclosureFeaturePlan';

CREATE INDEX PUBLIC.ppc_17970beb ON PUBLIC.ppc_proddisclosureftrpln_l(attributename_);

CREATE INDEX PUBLIC.ppc_745400 ON PUBLIC.ppc_proddisclosureftrpln_l(orderindex_);

CREATE INDEX PUBLIC.ppc_d25fe935 ON PUBLIC.ppc_proddisclosureftrpln_l(referencemodelid_);

CREATE INDEX PUBLIC.ppc_ba0fccd4 ON PUBLIC.ppc_proddisclosureftrpln_l(referencemodeltype_);

CREATE INDEX PUBLIC.ppc_556cf8 ON PUBLIC.ppc_proddisclosureftrpln_l(referencedbymodelid_);

CREATE INDEX PUBLIC.ppc_40b36bd7 ON PUBLIC.ppc_proddisclosureftrpln_l(referencedbymodeltype_);

CREATE INDEX PUBLIC.ppc_e0628d82 ON PUBLIC.ppc_proddisclosureftrpln_l(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_e0685167 ON PUBLIC.ppc_proddisclosureftrpln_l(name_);

CREATE INDEX PUBLIC.ppc_b4a89647 ON PUBLIC.ppc_proddisclosureftrpln_l(revision_);

CREATE TABLE PUBLIC.ppc_proddisclosureftrplnrcrd (enableflag_ NUMBER(1) DEFAULT 0, data_text_ VARCHAR(100), data_number_ NUMBER(3, 2) DEFAULT 0, id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, createdon_ TIMESTAMP NOT NULL, modifiedon_ TIMESTAMP NOT NULL, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_eabd84e5 PRIMARY KEY (id_));

CREATE INDEX PUBLIC.ppc_8db2100c ON PUBLIC.ppc_proddisclosureftrplnrcrd(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_8db7d3f1 ON PUBLIC.ppc_proddisclosureftrplnrcrd(name_);

CREATE INDEX PUBLIC.ppc_a1799a7d ON PUBLIC.ppc_proddisclosureftrplnrcrd(revision_);

CREATE TABLE PUBLIC.ppc_proddsclsrftrplnrcrd_l (attributename_ VARCHAR(64), orderindex_ NUMBER(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_fa56aae PRIMARY KEY (id_));

COMMENT ON TABLE PUBLIC.ppc_proddsclsrftrplnrcrd_l IS 'Association table for ProdDisclosureFeaturePlanRecord';

CREATE INDEX PUBLIC.ppc_e23b2c78 ON PUBLIC.ppc_proddsclsrftrplnrcrd_l(attributename_);

CREATE INDEX PUBLIC.ppc_8c45c693 ON PUBLIC.ppc_proddsclsrftrplnrcrd_l(orderindex_);

CREATE INDEX PUBLIC.ppc_6407e188 ON PUBLIC.ppc_proddsclsrftrplnrcrd_l(referencemodelid_);

CREATE INDEX PUBLIC.ppc_819afc67 ON PUBLIC.ppc_proddsclsrftrplnrcrd_l(referencemodeltype_);

CREATE INDEX PUBLIC.ppc_2a302fc5 ON PUBLIC.ppc_proddsclsrftrplnrcrd_l(referencedbymodelid_);

CREATE INDEX PUBLIC.ppc_5ee8af64 ON PUBLIC.ppc_proddsclsrftrplnrcrd_l(referencedbymodeltype_);

CREATE INDEX PUBLIC.ppc_b8962a55 ON PUBLIC.ppc_proddsclsrftrplnrcrd_l(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_b89bee3a ON PUBLIC.ppc_proddsclsrftrplnrcrd_l(name_);

CREATE INDEX PUBLIC.ppc_5f49314 ON PUBLIC.ppc_proddsclsrftrplnrcrd_l(revision_);

CREATE TABLE PUBLIC.ppc_proddisclosureftrrgnrcrd (enableflag_ NUMBER(1) DEFAULT 0, data_text_ VARCHAR(100), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, createdon_ TIMESTAMP NOT NULL, modifiedon_ TIMESTAMP NOT NULL, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_edfa3c1e PRIMARY KEY (id_));

CREATE INDEX PUBLIC.ppc_a8de82c5 ON PUBLIC.ppc_proddisclosureftrrgnrcrd(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_a8e446aa ON PUBLIC.ppc_proddisclosureftrrgnrcrd(name_);

CREATE INDEX PUBLIC.ppc_58076ca4 ON PUBLIC.ppc_proddisclosureftrrgnrcrd(revision_);

CREATE TABLE PUBLIC.ppc_proddsclsrftrrgnrcrd_l (attributename_ VARCHAR(64), orderindex_ NUMBER(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_369137a7 PRIMARY KEY (id_));

COMMENT ON TABLE PUBLIC.ppc_proddsclsrftrrgnrcrd_l IS 'Association table for ProdDisclosureFeatureRegionRecord';

CREATE INDEX PUBLIC.ppc_fe93fc1f ON PUBLIC.ppc_proddsclsrftrrgnrcrd_l(attributename_);

CREATE INDEX PUBLIC.ppc_5a54704c ON PUBLIC.ppc_proddsclsrftrrgnrcrd_l(orderindex_);

CREATE INDEX PUBLIC.ppc_27149c81 ON PUBLIC.ppc_proddsclsrftrrgnrcrd_l(referencemodelid_);

CREATE INDEX PUBLIC.ppc_b464dd20 ON PUBLIC.ppc_proddsclsrftrrgnrcrd_l(referencemodeltype_);

CREATE INDEX PUBLIC.ppc_50a2662c ON PUBLIC.ppc_proddsclsrftrrgnrcrd_l(referencedbymodelid_);

CREATE INDEX PUBLIC.ppc_b1a6e80b ON PUBLIC.ppc_proddsclsrftrrgnrcrd_l(referencedbymodeltype_);

CREATE INDEX PUBLIC.ppc_ba70d2ce ON PUBLIC.ppc_proddsclsrftrrgnrcrd_l(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_ba7696b3 ON PUBLIC.ppc_proddsclsrftrrgnrcrd_l(name_);

CREATE INDEX PUBLIC.ppc_5056777b ON PUBLIC.ppc_proddsclsrftrrgnrcrd_l(revision_);

CREATE TABLE PUBLIC.ppc_proddisclosurefeeplan (id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, createdon_ TIMESTAMP NOT NULL, modifiedon_ TIMESTAMP NOT NULL, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_99e05d6f PRIMARY KEY (id_));

CREATE INDEX PUBLIC.ppc_579a5bd5 ON PUBLIC.ppc_proddisclosurefeeplan(branchid_, temporalmodelid_);

CREATE INDEX PUBLIC.ppc_579a418e ON PUBLIC.ppc_proddisclosurefeeplan(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_57a00573 ON PUBLIC.ppc_proddisclosurefeeplan(name_);

CREATE INDEX PUBLIC.ppc_44e430bb ON PUBLIC.ppc_proddisclosurefeeplan(revision_);

CREATE TABLE PUBLIC.ppc_proddisclosurefeepln_l (attributename_ VARCHAR(64), orderindex_ NUMBER(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_b78d287d PRIMARY KEY (id_));

COMMENT ON TABLE PUBLIC.ppc_proddisclosurefeepln_l IS 'Association table for ProdDisclosureFeePlan';

CREATE INDEX PUBLIC.ppc_e9e49709 ON PUBLIC.ppc_proddisclosurefeepln_l(attributename_);

CREATE INDEX PUBLIC.ppc_fc51cda2 ON PUBLIC.ppc_proddisclosurefeepln_l(orderindex_);

CREATE INDEX PUBLIC.ppc_302157 ON PUBLIC.ppc_proddisclosurefeepln_l(referencemodelid_);

CREATE INDEX PUBLIC.ppc_b4b28476 ON PUBLIC.ppc_proddisclosurefeepln_l(referencemodeltype_);

CREATE INDEX PUBLIC.ppc_5a09a996 ON PUBLIC.ppc_proddisclosurefeepln_l(referencedbymodelid_);

CREATE INDEX PUBLIC.ppc_fe4af8f5 ON PUBLIC.ppc_proddisclosurefeepln_l(referencedbymodeltype_);

CREATE INDEX PUBLIC.ppc_87bc68a4 ON PUBLIC.ppc_proddisclosurefeepln_l(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_87c22c89 ON PUBLIC.ppc_proddisclosurefeepln_l(name_);

CREATE INDEX PUBLIC.ppc_56f5cee5 ON PUBLIC.ppc_proddisclosurefeepln_l(revision_);

CREATE TABLE PUBLIC.ppc_proddisclosurefeeplnrecord (enableflag_ NUMBER(1) DEFAULT 0, data_text_ VARCHAR(100), data_number_ NUMBER(3, 2) DEFAULT 0, id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, createdon_ TIMESTAMP NOT NULL, modifiedon_ TIMESTAMP NOT NULL, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_a2ebe599 PRIMARY KEY (id_));

CREATE INDEX PUBLIC.ppc_e6bef5c0 ON PUBLIC.ppc_proddisclosurefeeplnrecord(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_e6c4b9a5 ON PUBLIC.ppc_proddisclosurefeeplnrecord(name_);

CREATE INDEX PUBLIC.ppc_bd1f2e49 ON PUBLIC.ppc_proddisclosurefeeplnrecord(revision_);

CREATE TABLE PUBLIC.ppc_proddsclsrfplnrcrd_l (attributename_ VARCHAR(64), orderindex_ NUMBER(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_1349df30 PRIMARY KEY (id_));

COMMENT ON TABLE PUBLIC.ppc_proddsclsrfplnrcrd_l IS 'Association table for ProdDisclosureFeePlanRecord';

CREATE INDEX PUBLIC.ppc_9d523a36 ON PUBLIC.ppc_proddsclsrfplnrcrd_l(attributename_);

CREATE INDEX PUBLIC.ppc_2c69c195 ON PUBLIC.ppc_proddsclsrfplnrcrd_l(orderindex_);

CREATE INDEX PUBLIC.ppc_43d0130a ON PUBLIC.ppc_proddsclsrfplnrcrd_l(referencemodelid_);

CREATE INDEX PUBLIC.ppc_901cd569 ON PUBLIC.ppc_proddsclsrfplnrcrd_l(referencemodeltype_);

CREATE INDEX PUBLIC.ppc_ebe97703 ON PUBLIC.ppc_proddsclsrfplnrcrd_l(referencedbymodelid_);

CREATE INDEX PUBLIC.ppc_976d1f22 ON PUBLIC.ppc_proddsclsrfplnrcrd_l(referencedbymodeltype_);

CREATE INDEX PUBLIC.ppc_560b0ed7 ON PUBLIC.ppc_proddsclsrfplnrcrd_l(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_5610d2bc ON PUBLIC.ppc_proddsclsrfplnrcrd_l(name_);

CREATE INDEX PUBLIC.ppc_a7f01652 ON PUBLIC.ppc_proddsclsrfplnrcrd_l(revision_);

CREATE TABLE PUBLIC.ppc_proddisclosurefeergnrcrd (enableflag_ NUMBER(1) DEFAULT 0, data_text_ VARCHAR(100), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, createdon_ TIMESTAMP NOT NULL, modifiedon_ TIMESTAMP NOT NULL, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_eaf477c0 PRIMARY KEY (id_));

CREATE INDEX PUBLIC.ppc_e12e1d67 ON PUBLIC.ppc_proddisclosurefeergnrcrd(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_e133e14c ON PUBLIC.ppc_proddisclosurefeergnrcrd(name_);

CREATE INDEX PUBLIC.ppc_9be8f5c2 ON PUBLIC.ppc_proddisclosurefeergnrcrd(revision_);

CREATE TABLE PUBLIC.ppc_proddsclsrfrgnrcrd_l (attributename_ VARCHAR(64), orderindex_ NUMBER(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_3a35ac29 PRIMARY KEY (id_));

COMMENT ON TABLE PUBLIC.ppc_proddsclsrfrgnrcrd_l IS 'Association table for ProdDisclosureFeeRegionRecord';

CREATE INDEX PUBLIC.ppc_b9ab09dd ON PUBLIC.ppc_proddsclsrfrgnrcrd_l(attributename_);

CREATE INDEX PUBLIC.ppc_fa786b4e ON PUBLIC.ppc_proddsclsrfrgnrcrd_l(orderindex_);

CREATE INDEX PUBLIC.ppc_6dcce03 ON PUBLIC.ppc_proddsclsrfrgnrcrd_l(referencemodelid_);

CREATE INDEX PUBLIC.ppc_c2e6b622 ON PUBLIC.ppc_proddsclsrfrgnrcrd_l(referencemodeltype_);

CREATE INDEX PUBLIC.ppc_125bad6a ON PUBLIC.ppc_proddsclsrfrgnrcrd_l(referencedbymodelid_);

CREATE INDEX PUBLIC.ppc_ea2b57c9 ON PUBLIC.ppc_proddsclsrfrgnrcrd_l(referencedbymodeltype_);

CREATE INDEX PUBLIC.ppc_57e5b750 ON PUBLIC.ppc_proddsclsrfrgnrcrd_l(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_57eb7b35 ON PUBLIC.ppc_proddsclsrfrgnrcrd_l(name_);

CREATE INDEX PUBLIC.ppc_f251fab9 ON PUBLIC.ppc_proddsclsrfrgnrcrd_l(revision_);

CREATE TABLE PUBLIC.ppc_proddisclosureformplan (id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, createdon_ TIMESTAMP NOT NULL, modifiedon_ TIMESTAMP NOT NULL, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_628e03a9 PRIMARY KEY (id_));

CREATE INDEX PUBLIC.ppc_c343e817 ON PUBLIC.ppc_proddisclosureformplan(branchid_, temporalmodelid_);

CREATE INDEX PUBLIC.ppc_c343cdd0 ON PUBLIC.ppc_proddisclosureformplan(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_c34991b5 ON PUBLIC.ppc_proddisclosureformplan(name_);

CREATE INDEX PUBLIC.ppc_2f971439 ON PUBLIC.ppc_proddisclosureformplan(revision_);

CREATE TABLE PUBLIC.ppc_proddisclosureformpln_l (attributename_ VARCHAR(64), orderindex_ NUMBER(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_4944983 PRIMARY KEY (id_));

COMMENT ON TABLE PUBLIC.ppc_proddisclosureformpln_l IS 'Association table for ProdDisclosureFormPlan';

CREATE INDEX PUBLIC.ppc_b0538cb ON PUBLIC.ppc_proddisclosureformpln_l(attributename_);

CREATE INDEX PUBLIC.ppc_bd205320 ON PUBLIC.ppc_proddisclosureformpln_l(orderindex_);

CREATE INDEX PUBLIC.ppc_14981055 ON PUBLIC.ppc_proddisclosureformpln_l(referencemodelid_);

CREATE INDEX PUBLIC.ppc_4edaabf4 ON PUBLIC.ppc_proddisclosureformpln_l(referencemodeltype_);

CREATE INDEX PUBLIC.ppc_4e671d8 ON PUBLIC.ppc_proddisclosureformpln_l(referencedbymodelid_);

CREATE INDEX PUBLIC.ppc_6516b8b7 ON PUBLIC.ppc_proddisclosureformpln_l(referencedbymodeltype_);

CREATE INDEX PUBLIC.ppc_914464a2 ON PUBLIC.ppc_proddisclosureformpln_l(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_914a2887 ON PUBLIC.ppc_proddisclosureformpln_l(name_);

CREATE INDEX PUBLIC.ppc_c29f5b27 ON PUBLIC.ppc_proddisclosureformpln_l(revision_);

CREATE TABLE PUBLIC.ppc_proddisclosureformplanrcrd (printing_option_ VARCHAR(256), enableflag_ NUMBER(1) DEFAULT 0, data_text_ VARCHAR(100), data_number_ NUMBER(3, 2) DEFAULT 0, id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, createdon_ TIMESTAMP NOT NULL, modifiedon_ TIMESTAMP NOT NULL, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_b98a9926 PRIMARY KEY (id_));

CREATE INDEX PUBLIC.ppc_608f1acd ON PUBLIC.ppc_proddisclosureformplanrcrd(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_6094deb2 ON PUBLIC.ppc_proddisclosureformplanrcrd(name_);

CREATE INDEX PUBLIC.ppc_b5fb39c ON PUBLIC.ppc_proddisclosureformplanrcrd(revision_);

CREATE TABLE PUBLIC.ppc_proddsclsrfrmplnrcrd_l (attributename_ VARCHAR(64), orderindex_ NUMBER(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_a35ab6eb PRIMARY KEY (id_));

COMMENT ON TABLE PUBLIC.ppc_proddsclsrfrmplnrcrd_l IS 'Association table for ProdDisclosureFormPlanRecord';

CREATE INDEX PUBLIC.ppc_665a6b5b ON PUBLIC.ppc_proddsclsrfrmplnrcrd_l(attributename_);

CREATE INDEX PUBLIC.ppc_214aaa90 ON PUBLIC.ppc_proddsclsrfrmplnrcrd_l(orderindex_);

CREATE INDEX PUBLIC.ppc_941f13c5 ON PUBLIC.ppc_proddsclsrfrmplnrcrd_l(referencemodelid_);

CREATE INDEX PUBLIC.ppc_8ae9364 ON PUBLIC.ppc_proddsclsrfrmplnrcrd_l(referencemodeltype_);

CREATE INDEX PUBLIC.ppc_858f7868 ON PUBLIC.ppc_proddsclsrfrmplnrcrd_l(referencedbymodelid_);

CREATE INDEX PUBLIC.ppc_5f985b47 ON PUBLIC.ppc_proddsclsrfrmplnrcrd_l(referencedbymodeltype_);

CREATE INDEX PUBLIC.ppc_9c211012 ON PUBLIC.ppc_proddsclsrfrmplnrcrd_l(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_9c26d3f7 ON PUBLIC.ppc_proddsclsrfrmplnrcrd_l(name_);

CREATE INDEX PUBLIC.ppc_fcc781b7 ON PUBLIC.ppc_proddsclsrfrmplnrcrd_l(revision_);

CREATE TABLE PUBLIC.ppc_proddisclosurewaiverrecord (enableflag_ NUMBER(1) DEFAULT 0, data_text_ VARCHAR(100), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, createdon_ TIMESTAMP NOT NULL, modifiedon_ TIMESTAMP NOT NULL, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_ba21a8e1 PRIMARY KEY (id_));

CREATE INDEX PUBLIC.ppc_1b51d508 ON PUBLIC.ppc_proddisclosurewaiverrecord(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_1b5798ed ON PUBLIC.ppc_proddisclosurewaiverrecord(name_);

CREATE INDEX PUBLIC.ppc_b68f4601 ON PUBLIC.ppc_proddisclosurewaiverrecord(revision_);

CREATE TABLE PUBLIC.ppc_proddisclosurewvrrcrd_l (attributename_ VARCHAR(64), orderindex_ NUMBER(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_64dcf91b PRIMARY KEY (id_));

COMMENT ON TABLE PUBLIC.ppc_proddisclosurewvrrcrd_l IS 'Association table for ProdDisclosureWaiverRecord';

CREATE INDEX PUBLIC.ppc_1840a033 ON PUBLIC.ppc_proddisclosurewvrrcrd_l(attributename_);

CREATE INDEX PUBLIC.ppc_216464b8 ON PUBLIC.ppc_proddisclosurewvrrcrd_l(orderindex_);

CREATE INDEX PUBLIC.ppc_e87e8fed ON PUBLIC.ppc_proddisclosurewvrrcrd_l(referencemodelid_);

CREATE INDEX PUBLIC.ppc_c31fa58c ON PUBLIC.ppc_proddisclosurewvrrcrd_l(referencemodeltype_);

CREATE INDEX PUBLIC.ppc_1940ab40 ON PUBLIC.ppc_proddisclosurewvrrcrd_l(referencedbymodelid_);

CREATE INDEX PUBLIC.ppc_cbc8381f ON PUBLIC.ppc_proddisclosurewvrrcrd_l(referencedbymodeltype_);

CREATE INDEX PUBLIC.ppc_37fb883a ON PUBLIC.ppc_proddisclosurewvrrcrd_l(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_38014c1f ON PUBLIC.ppc_proddisclosurewvrrcrd_l(name_);

CREATE INDEX PUBLIC.ppc_6d29648f ON PUBLIC.ppc_proddisclosurewvrrcrd_l(revision_);

CREATE TABLE PUBLIC.ppc_proddisclosurewvrrgnrcrd (enableflag_ NUMBER(1) DEFAULT 0, data_text_ VARCHAR(100), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, createdon_ TIMESTAMP NOT NULL, modifiedon_ TIMESTAMP NOT NULL, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_d410f56d PRIMARY KEY (id_));

CREATE INDEX PUBLIC.ppc_c749fc94 ON PUBLIC.ppc_proddisclosurewvrrgnrcrd(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_c74fc079 ON PUBLIC.ppc_proddisclosurewvrrgnrcrd(name_);

CREATE INDEX PUBLIC.ppc_66dfcf5 ON PUBLIC.ppc_proddisclosurewvrrgnrcrd(revision_);

CREATE TABLE PUBLIC.ppc_proddsclsrwvrrgnrcrd_l (attributename_ VARCHAR(64), orderindex_ NUMBER(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_f1ded936 PRIMARY KEY (id_));

COMMENT ON TABLE PUBLIC.ppc_proddsclsrwvrrgnrcrd_l IS 'Association table for ProdDisclosureWaiverRegionRecord';

CREATE INDEX PUBLIC.ppc_6f179af0 ON PUBLIC.ppc_proddsclsrwvrrgnrcrd_l(attributename_);

CREATE INDEX PUBLIC.ppc_198cab1b ON PUBLIC.ppc_proddsclsrwvrrgnrcrd_l(orderindex_);

CREATE INDEX PUBLIC.ppc_8be34010 ON PUBLIC.ppc_proddsclsrwvrrgnrcrd_l(referencemodelid_);

CREATE INDEX PUBLIC.ppc_2018d8ef ON PUBLIC.ppc_proddsclsrwvrrgnrcrd_l(referencemodeltype_);

CREATE INDEX PUBLIC.ppc_5b6de43d ON PUBLIC.ppc_proddsclsrwvrrgnrcrd_l(referencedbymodelid_);

CREATE INDEX PUBLIC.ppc_378b25dc ON PUBLIC.ppc_proddsclsrwvrrgnrcrd_l(referencedbymodeltype_);

CREATE INDEX PUBLIC.ppc_ebe514dd ON PUBLIC.ppc_proddsclsrwvrrgnrcrd_l(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_ebead8c2 ON PUBLIC.ppc_proddsclsrwvrrgnrcrd_l(name_);

CREATE INDEX PUBLIC.ppc_ff5a378c ON PUBLIC.ppc_proddsclsrwvrrgnrcrd_l(revision_);

CREATE TABLE PUBLIC.ppc_prodorganizationalstrctr (id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, createdon_ TIMESTAMP NOT NULL, modifiedon_ TIMESTAMP NOT NULL, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_3cdf0355 PRIMARY KEY (id_));

CREATE INDEX PUBLIC.ppc_dd0ef0c3 ON PUBLIC.ppc_prodorganizationalstrctr(branchid_, temporalmodelid_);

CREATE INDEX PUBLIC.ppc_dd0ed67c ON PUBLIC.ppc_prodorganizationalstrctr(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_dd149a61 ON PUBLIC.ppc_prodorganizationalstrctr(name_);

CREATE INDEX PUBLIC.ppc_4932460d ON PUBLIC.ppc_prodorganizationalstrctr(revision_);

CREATE TABLE PUBLIC.ppc_prodorgnztnlstrctr_l (attributename_ VARCHAR(64), orderindex_ NUMBER(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ NUMBER(1) DEFAULT 0, revision_ NUMBER(24) DEFAULT 0 NOT NULL, parentrevision_ NUMBER(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP, businessdateto_ TIMESTAMP, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_68c56e4 PRIMARY KEY (id_));

COMMENT ON TABLE PUBLIC.ppc_prodorgnztnlstrctr_l IS 'Association table for ProdOrganizationalStructure';

CREATE INDEX PUBLIC.ppc_6dfae702 ON PUBLIC.ppc_prodorgnztnlstrctr_l(attributename_);

CREATE INDEX PUBLIC.ppc_75112849 ON PUBLIC.ppc_prodorgnztnlstrctr_l(orderindex_);

CREATE INDEX PUBLIC.ppc_20b4a2be ON PUBLIC.ppc_prodorgnztnlstrctr_l(referencemodelid_);

CREATE INDEX PUBLIC.ppc_c61c481d ON PUBLIC.ppc_prodorgnztnlstrctr_l(referencemodeltype_);

CREATE INDEX PUBLIC.ppc_75d85acf ON PUBLIC.ppc_prodorgnztnlstrctr_l(referencedbymodelid_);

CREATE INDEX PUBLIC.ppc_61323fee ON PUBLIC.ppc_prodorgnztnlstrctr_l(referencedbymodeltype_);

CREATE INDEX PUBLIC.ppc_6f228c8b ON PUBLIC.ppc_prodorgnztnlstrctr_l(branchid_, modelid_);

CREATE INDEX PUBLIC.ppc_6f285070 ON PUBLIC.ppc_prodorgnztnlstrctr_l(name_);

CREATE INDEX PUBLIC.ppc_bd8121e ON PUBLIC.ppc_prodorgnztnlstrctr_l(revision_);

INSERT INTO PUBLIC.core_databasechangelog (tag_, author_, md5sum_, modulename_, id_, modelid_, modeltype_) VALUES ('0.0.1', 'system', '9c97ce1a5073034eff57204bb35cf314', 'ppc', '6b0bd1f3-0a7f-43be-bcc4-fb17a0d3ace7', 'b8660e03-a580-4703-bc29-a9519f2fb53b', 'com.zafin.zplatform.models.DatabaseChangelog');

