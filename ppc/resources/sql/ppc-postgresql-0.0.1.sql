-- *********************************************************************
-- Update Database Script
-- *********************************************************************
-- Change Log: xml
-- Ran at: 1/8/20 2:54 PM
-- Against: System@offline:postgresql
-- Liquibase version: 3.5.0
-- *********************************************************************

-- Changeset xml::Revision #0.0.1::Modeling Workbench
CREATE TABLE ppc_bankproduct (code_ VARCHAR(3), associated_apps_ VARCHAR(100), account_prefix_ VARCHAR(150), account_format_ VARCHAR(100), interfaces_ VARCHAR(150), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_c55f47e7" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

CREATE INDEX ppc_f82dd48 ON ppc_bankproduct(code_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_8f538b06 ON ppc_bankproduct(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_8f594eeb ON ppc_bankproduct(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_49fd843 ON ppc_bankproduct(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_bankproduct_l (attributename_ VARCHAR(64), orderindex_ numeric(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_eabcc99a" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

COMMENT ON TABLE ppc_bankproduct_l IS 'Association table for BankProduct';

CREATE INDEX ppc_9be9b14 ON ppc_bankproduct_l(attributename_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_cdce3d77 ON ppc_bankproduct_l(orderindex_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_9bec9d6c ON ppc_bankproduct_l(referencemodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_53404f4b ON ppc_bankproduct_l(referencemodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_8d353961 ON ppc_bankproduct_l(referencedbymodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_14d1c200 ON ppc_bankproduct_l(referencedbymodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_bea9c39 ON ppc_bankproduct_l(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_bf0601e ON ppc_bankproduct_l(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_d71854b0 ON ppc_bankproduct_l(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_banksubproduct (code_ VARCHAR(2), market_type_ VARCHAR(4), iban_retl_comm_ind_ VARCHAR(1), iban_stmt_format_ VARCHAR(3), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_2bc165ef" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

CREATE INDEX ppc_8c242116 ON ppc_banksubproduct(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_8c29e4fb ON ppc_banksubproduct(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_5f5c4033 ON ppc_banksubproduct(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_banksubproduct_l (attributename_ VARCHAR(64), orderindex_ numeric(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_410f85a2" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

COMMENT ON TABLE ppc_banksubproduct_l IS 'Association table for BankSubProduct';

CREATE INDEX ppc_7cee9b04 ON ppc_banksubproduct_l(attributename_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_57efc387 ON ppc_banksubproduct_l(orderindex_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_1ab5577c ON ppc_banksubproduct_l(referencemodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_42c2c55b ON ppc_banksubproduct_l(referencemodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_8e018551 ON ppc_banksubproduct_l(referencedbymodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_13bad1f0 ON ppc_banksubproduct_l(referencedbymodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_16edee49 ON ppc_banksubproduct_l(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_16f3b22e ON ppc_banksubproduct_l(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_745a80a0 ON ppc_banksubproduct_l(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_bankingchannel (code_ VARCHAR(16), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_b1ac847d" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

CREATE INDEX ppc_25676c66 ON ppc_bankingchannel(code_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_d32148a4 ON ppc_bankingchannel(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_d3270c89 ON ppc_bankingchannel(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_9b74eee5 ON ppc_bankingchannel(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_bankingchannel_l (attributename_ VARCHAR(64), orderindex_ numeric(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_f8ad38b0" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

COMMENT ON TABLE ppc_bankingchannel_l IS 'Association table for BankingChannel';

CREATE INDEX ppc_3a8cceb6 ON ppc_bankingchannel_l(attributename_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_fccb7d15 ON ppc_bankingchannel_l(orderindex_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_27d72e8a ON ppc_bankingchannel_l(referencemodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_8ecb10e9 ON ppc_bankingchannel_l(referencemodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_c302ab83 ON ppc_bankingchannel_l(referencedbymodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_d0b33a2 ON ppc_bankingchannel_l(referencedbymodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_933f6a57 ON ppc_bankingchannel_l(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_93452e3c ON ppc_bankingchannel_l(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_d024ad2 ON ppc_bankingchannel_l(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_bankinglineofbusiness (code_ VARCHAR(16), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_6bd38f29" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

CREATE INDEX ppc_64c5f0a ON ppc_bankinglineofbusiness(code_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_b4ca0648 ON ppc_bankinglineofbusiness(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_b4cfca2d ON ppc_bankinglineofbusiness(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_60abecc1 ON ppc_bankinglineofbusiness(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_bankinglineofbiz_l (attributename_ VARCHAR(64), orderindex_ numeric(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_2c402eeb" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

COMMENT ON TABLE ppc_bankinglineofbiz_l IS 'Association table for BankingLineOfBusiness';

CREATE INDEX ppc_a1b0f15b ON ppc_bankinglineofbiz_l(attributename_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_4af1e490 ON ppc_bankinglineofbiz_l(orderindex_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_cdf2cdc5 ON ppc_bankinglineofbiz_l(referencemodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_1c7bcd64 ON ppc_bankinglineofbiz_l(referencemodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_eb697e68 ON ppc_bankinglineofbiz_l(referencedbymodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_b708e147 ON ppc_bankinglineofbiz_l(referencedbymodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_fa3bca12 ON ppc_bankinglineofbiz_l(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_fa418df7 ON ppc_bankinglineofbiz_l(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_736d87b7 ON ppc_bankinglineofbiz_l(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_bankingregion (code_ VARCHAR(4), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_1e643320" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

CREATE INDEX ppc_1e94b641 ON ppc_bankingregion(code_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_e315d2bf ON ppc_bankingregion(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_e31b96a4 ON ppc_bankingregion(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_de09356a ON ppc_bankingregion(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_bankingregion_l (attributename_ VARCHAR(64), orderindex_ numeric(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_1633ca93" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

COMMENT ON TABLE ppc_bankingregion_l IS 'Association table for BankingRegion';

CREATE INDEX ppc_fcfa75bb ON ppc_bankingregion_l(attributename_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_5fcdbc30 ON ppc_bankingregion_l(orderindex_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_fe366d65 ON ppc_bankingregion_l(referencemodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_4a560504 ON ppc_bankingregion_l(referencemodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_78d63ac8 ON ppc_bankingregion_l(referencedbymodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_9c3805a7 ON ppc_bankingregion_l(referencedbymodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_7839d9b2 ON ppc_bankingregion_l(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_783f9d97 ON ppc_bankingregion_l(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_fb9f0417 ON ppc_bankingregion_l(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_disclosurefee (code_ VARCHAR(16), fieldtype_ VARCHAR(256), is_regionalized_ numeric(1) DEFAULT 1, id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_58cb7ee1" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

CREATE INDEX ppc_50504c2 ON ppc_disclosurefee(code_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_bd7f5600 ON ppc_disclosurefee(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_bd8519e5 ON ppc_disclosurefee(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_58b1c609 ON ppc_disclosurefee(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_disclosurefee_l (attributename_ VARCHAR(64), orderindex_ numeric(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_53f72a14" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

COMMENT ON TABLE ppc_disclosurefee_l IS 'Association table for DisclosureFee';

CREATE INDEX ppc_92ac0fda ON ppc_disclosurefee_l(attributename_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_6b646e71 ON ppc_disclosurefee_l(orderindex_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_4f4aae6 ON ppc_disclosurefee_l(referencemodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_9a7ae645 ON ppc_disclosurefee_l(referencemodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_2d4d81a7 ON ppc_disclosurefee_l(referencedbymodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_ff910c6 ON ppc_disclosurefee_l(referencedbymodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_5e4f90b3 ON ppc_disclosurefee_l(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_5e555498 ON ppc_disclosurefee_l(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_6e65e8f6 ON ppc_disclosurefee_l(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_disclosurefeeperiod (id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_f74a8440" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

CREATE INDEX ppc_d62c1126 ON ppc_disclosurefeeperiod(branchid_, temporalmodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_d62bf6df ON ppc_disclosurefeeperiod(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_d631bac4 ON ppc_disclosurefeeperiod(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_9b8e0d4a ON ppc_disclosurefeeperiod(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_disclosurefeeperiod_l (attributename_ VARCHAR(64), orderindex_ numeric(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_4eca53b3" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

COMMENT ON TABLE ppc_disclosurefeeperiod_l IS 'Association table for DisclosureFeePeriod';

CREATE INDEX ppc_519a7d9b ON ppc_disclosurefeeperiod_l(attributename_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_50dc050 ON ppc_disclosurefeeperiod_l(orderindex_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_e92ad985 ON ppc_disclosurefeeperiod_l(referencemodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_49dfe924 ON ppc_disclosurefeeperiod_l(referencemodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_6a88daa8 ON ppc_disclosurefeeperiod_l(referencedbymodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_ebc22d87 ON ppc_disclosurefeeperiod_l(referencedbymodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_fe5775d2 ON ppc_disclosurefeeperiod_l(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_fe5d39b7 ON ppc_disclosurefeeperiod_l(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_6b4d63f7 ON ppc_disclosurefeeperiod_l(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_disclosurefeeplan (code_ VARCHAR(16), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_b64bdb98" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

CREATE INDEX ppc_c0c72ab9 ON ppc_disclosurefeeplan(code_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_e7770137 ON ppc_disclosurefeeplan(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_e77cc51c ON ppc_disclosurefeeplan(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_5a0c57f2 ON ppc_disclosurefeeplan(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_disclosurefeeplan_l (attributename_ VARCHAR(64), orderindex_ numeric(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_52d3350b" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

COMMENT ON TABLE ppc_disclosurefeeplan_l IS 'Association table for DisclosureFeePlan';

CREATE INDEX ppc_a8f60c43 ON ppc_disclosurefeeplan_l(attributename_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_3f34f2a8 ON ppc_disclosurefeeplan_l(orderindex_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_d0c8e9dd ON ppc_disclosurefeeplan_l(referencemodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_c23b437c ON ppc_disclosurefeeplan_l(referencemodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_fd98cb50 ON ppc_disclosurefeeplan_l(referencedbymodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_fa98942f ON ppc_disclosurefeeplan_l(referencedbymodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_e9094a2a ON ppc_disclosurefeeplan_l(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_e90f0e0f ON ppc_disclosurefeeplan_l(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_8363a49f ON ppc_disclosurefeeplan_l(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_disclosureform (code_ VARCHAR(16), printing_option_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_d272b1e9" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

CREATE INDEX ppc_8a94c2d2 ON ppc_disclosureform(code_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_a0fae010 ON ppc_disclosureform(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_a100a3f5 ON ppc_disclosureform(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_9194b9f9 ON ppc_disclosureform(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_disclosureform_l (attributename_ VARCHAR(64), orderindex_ numeric(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_9dbb1c" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

COMMENT ON TABLE ppc_disclosureform_l IS 'Association table for DisclosureForm';

CREATE INDEX ppc_82909bca ON ppc_disclosureform_l(attributename_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_cbfce881 ON ppc_disclosureform_l(orderindex_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_9a2d58f6 ON ppc_disclosureform_l(referencemodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_c4405055 ON ppc_disclosureform_l(referencemodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_3c355997 ON ppc_disclosureform_l(referencedbymodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_44aacb6 ON ppc_disclosureform_l(referencedbymodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_5110d6c3 ON ppc_disclosureform_l(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_51169aa8 ON ppc_disclosureform_l(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_fa5ba0e6 ON ppc_disclosureform_l(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_disclosureformperiod (id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_dcb15148" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

CREATE INDEX ppc_5bbbcf36 ON ppc_disclosureformperiod(branchid_, temporalmodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_5bbbb4ef ON ppc_disclosureformperiod(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_5bc178d4 ON ppc_disclosureformperiod(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_45064d3a ON ppc_disclosureformperiod(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_disclosureformperiod_l (attributename_ VARCHAR(64), orderindex_ numeric(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_75b1febb" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

COMMENT ON TABLE ppc_disclosureformperiod_l IS 'Association table for DisclosureFormPeriod';

CREATE INDEX ppc_6082558b ON ppc_disclosureformperiod_l(attributename_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_9a466e60 ON ppc_disclosureformperiod_l(orderindex_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_7f0cbb95 ON ppc_disclosureformperiod_l(referencemodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_eed18734 ON ppc_disclosureformperiod_l(referencemodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_63cafe98 ON ppc_disclosureformperiod_l(referencedbymodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_9d0b1577 ON ppc_disclosureformperiod_l(referencedbymodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_5eefefe2 ON ppc_disclosureformperiod_l(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_5ef5b3c7 ON ppc_disclosureformperiod_l(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_97b567e7 ON ppc_disclosureformperiod_l(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_disclosureformplan (code_ VARCHAR(16), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_d3924aa0" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

CREATE INDEX ppc_5d2960c9 ON ppc_disclosureformplan(code_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_2eff0347 ON ppc_disclosureformplan(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_2f04c72c ON ppc_disclosureformplan(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_bf73d3e2 ON ppc_disclosureformplan(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_disclosureformplan_l (attributename_ VARCHAR(64), orderindex_ numeric(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_383a0213" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

COMMENT ON TABLE ppc_disclosureformplan_l IS 'Association table for DisclosureFormPlan';

CREATE INDEX ppc_bad32033 ON ppc_disclosureformplan_l(attributename_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_4be1e4b8 ON ppc_disclosureformplan_l(orderindex_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_9edc0fed ON ppc_disclosureformplan_l(referencemodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_581d258c ON ppc_disclosureformplan_l(referencemodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_23f32b40 ON ppc_disclosureformplan_l(referencedbymodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_f3dab81f ON ppc_disclosureformplan_l(referencedbymodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_6e99083a ON ppc_disclosureformplan_l(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_6e9ecc1f ON ppc_disclosureformplan_l(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_2cdbe48f ON ppc_disclosureformplan_l(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_disclosurewaiver (code_ VARCHAR(16), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_5e737589" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

CREATE INDEX ppc_b7aafd72 ON ppc_disclosurewaiver(code_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_ff7992b0 ON ppc_disclosurewaiver(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_ff7f5695 ON ppc_disclosurewaiver(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_a5253359 ON ppc_disclosurewaiver(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_disclosurewaiver_l (attributename_ VARCHAR(64), orderindex_ numeric(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_8f7c16bc" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

COMMENT ON TABLE ppc_disclosurewaiver_l IS 'Association table for DisclosureWaiver';

CREATE INDEX ppc_1d56052a ON ppc_disclosurewaiver_l(attributename_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_c6efb21 ON ppc_disclosurewaiver_l(orderindex_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_7d2ef396 ON ppc_disclosurewaiver_l(referencemodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_ed45c2f5 ON ppc_disclosurewaiver_l(referencemodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_33de3af7 ON ppc_disclosurewaiver_l(referencedbymodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_b540b616 ON ppc_disclosurewaiver_l(referencedbymodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_aad6163 ON ppc_disclosurewaiver_l(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_ab32548 ON ppc_disclosurewaiver_l(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_6bb34246 ON ppc_disclosurewaiver_l(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_featuresandservices (code_ VARCHAR(16), fieldtype_ VARCHAR(256), is_regionalized_ numeric(1) DEFAULT 1, id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_a1f00e22" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

CREATE INDEX ppc_1c3b59c3 ON ppc_featuresandservices(code_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_3d55bfc1 ON ppc_featuresandservices(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_3d5b83a6 ON ppc_featuresandservices(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_f176a028 ON ppc_featuresandservices(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_featuresandservices_l (attributename_ VARCHAR(64), orderindex_ numeric(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_e634ed15" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

COMMENT ON TABLE ppc_featuresandservices_l IS 'Association table for FeaturesAndServices';

CREATE INDEX ppc_9ad7b379 ON ppc_featuresandservices_l(attributename_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_8e598732 ON ppc_featuresandservices_l(orderindex_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_d34a6ee7 ON ppc_featuresandservices_l(referencemodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_2a6fae06 ON ppc_featuresandservices_l(referencemodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_9bf1b206 ON ppc_featuresandservices_l(referencedbymodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_6652a565 ON ppc_featuresandservices_l(referencedbymodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_42328e34 ON ppc_featuresandservices_l(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_42385219 ON ppc_featuresandservices_l(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_e95cb755 ON ppc_featuresandservices_l(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_featuresandservicesplan (code_ VARCHAR(16), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_b024e259" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

CREATE INDEX ppc_fad9173a ON ppc_featuresandservicesplan(code_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_5e90a278 ON ppc_featuresandservicesplan(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_5e96665d ON ppc_featuresandservicesplan(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_f2890a91 ON ppc_featuresandservicesplan(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_featuresandsrvcspln_l (attributename_ VARCHAR(64), orderindex_ numeric(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_f738b9a8" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

COMMENT ON TABLE ppc_featuresandsrvcspln_l IS 'Association table for FeaturesAndServicesPlan';

CREATE INDEX ppc_f5ba2dc6 ON ppc_featuresandsrvcspln_l(attributename_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_3560f805 ON ppc_featuresandsrvcspln_l(orderindex_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_2cc0b57a ON ppc_featuresandsrvcspln_l(referencemodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_ff6e9bd9 ON ppc_featuresandsrvcspln_l(referencemodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_66d07e93 ON ppc_featuresandsrvcspln_l(referencedbymodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_f4b082b2 ON ppc_featuresandsrvcspln_l(referencedbymodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_5c87d947 ON ppc_featuresandsrvcspln_l(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_5c8d9d2c ON ppc_featuresandsrvcspln_l(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_f2783de2 ON ppc_featuresandsrvcspln_l(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_featuresandservicestmprl (id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_42b99f61" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

CREATE INDEX ppc_fce4a6cf ON ppc_featuresandservicestmprl(branchid_, temporalmodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_fce48c88 ON ppc_featuresandservicestmprl(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_fcea506d ON ppc_featuresandservicestmprl(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_84079e81 ON ppc_featuresandservicestmprl(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_featuresandsrvcstmprl_l (attributename_ VARCHAR(64), orderindex_ numeric(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_2e43c229" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

COMMENT ON TABLE ppc_featuresandsrvcstmprl_l IS 'Association table for FeaturesAndServicesTemporal';

CREATE INDEX ppc_c2097de5 ON ppc_featuresandsrvcstmprl_l(attributename_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_9825b446 ON ppc_featuresandsrvcstmprl_l(orderindex_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_ee7d7cfb ON ppc_featuresandsrvcstmprl_l(referencemodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_4517871a ON ppc_featuresandsrvcstmprl_l(referencemodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_d644fb72 ON ppc_featuresandsrvcstmprl_l(referencedbymodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_58f943d1 ON ppc_featuresandsrvcstmprl_l(referencedbymodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_ef9b1a48 ON ppc_featuresandsrvcstmprl_l(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_efa0de2d ON ppc_featuresandsrvcstmprl_l(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_bd6858c1 ON ppc_featuresandsrvcstmprl_l(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_offeringstatus (code_ VARCHAR(16), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_a7b98d42" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

CREATE INDEX ppc_68c802eb ON ppc_offeringstatus(code_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_764266e9 ON ppc_offeringstatus(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_76482ace ON ppc_offeringstatus(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_d2447400 ON ppc_offeringstatus(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_offeringstatus_l (attributename_ VARCHAR(64), orderindex_ numeric(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_9f9b2435" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

COMMENT ON TABLE ppc_offeringstatus_l IS 'Association table for OfferingStatus';

CREATE INDEX ppc_ed0e8351 ON ppc_offeringstatus_l(attributename_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_ea60865a ON ppc_offeringstatus_l(orderindex_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_2bcf700f ON ppc_offeringstatus_l(referencemodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_75b9052e ON ppc_offeringstatus_l(referencemodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_b9d33fde ON ppc_offeringstatus_l(referencedbymodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_92081d3d ON ppc_offeringstatus_l(referencedbymodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_f2920b5c ON ppc_offeringstatus_l(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_f297cf41 ON ppc_offeringstatus_l(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_ce04f52d ON ppc_offeringstatus_l(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_ppcproduct (code_ VARCHAR(6), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_d4e3bf0e" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

CREATE INDEX ppc_ea0127b7 ON ppc_ppcproduct(code_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_f3a14b5 ON ppc_ppcproduct(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_f3fd89a ON ppc_ppcproduct(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_eff9dcb4 ON ppc_ppcproduct(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_ppcproduct_l (attributename_ VARCHAR(64), orderindex_ numeric(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_2b001301" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

COMMENT ON TABLE ppc_ppcproduct_l IS 'Association table for PpcProduct';

CREATE INDEX ppc_12087e05 ON ppc_ppcproduct_l(attributename_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_1e3a826 ON ppc_ppcproduct_l(orderindex_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_2a2d08db ON ppc_ppcproduct_l(referencemodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_53139afa ON ppc_ppcproduct_l(referencemodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_87cb6392 ON ppc_ppcproduct_l(referencedbymodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_c28623f1 ON ppc_ppcproduct_l(referencedbymodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_2c557628 ON ppc_ppcproduct_l(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_2c5b3a0d ON ppc_ppcproduct_l(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_540300e1 ON ppc_ppcproduct_l(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_prodavailability (id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_9d8b848" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

CREATE INDEX ppc_1cf17836 ON ppc_prodavailability(branchid_, temporalmodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_1cf15def ON ppc_prodavailability(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_1cf721d4 ON ppc_prodavailability(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_21f7843a ON ppc_prodavailability(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_prodavailability_l (attributename_ VARCHAR(64), orderindex_ numeric(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_f69ba5bb" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

COMMENT ON TABLE ppc_prodavailability_l IS 'Association table for ProdAvailability';

CREATE INDEX ppc_82590c8b ON ppc_prodavailability_l(attributename_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_83c31760 ON ppc_prodavailability_l(orderindex_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_58a6a495 ON ppc_prodavailability_l(referencemodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_c9953034 ON ppc_prodavailability_l(referencemodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_e17c7598 ON ppc_prodavailability_l(referencedbymodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_743acc77 ON ppc_prodavailability_l(referencedbymodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_a95f58e2 ON ppc_prodavailability_l(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_a9651cc7 ON ppc_prodavailability_l(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_fd34dee7 ON ppc_prodavailability_l(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_proddisclosurefeatureplan (id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_8a94bdff" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

CREATE INDEX ppc_3f5c7665 ON ppc_proddisclosurefeatureplan(branchid_, temporalmodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_3f5c5c1e ON ppc_proddisclosurefeatureplan(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_3f622003 ON ppc_proddisclosurefeatureplan(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_862c842b ON ppc_proddisclosurefeatureplan(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_proddisclosureftrpln_l (attributename_ VARCHAR(64), orderindex_ numeric(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_8f546c5b" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

COMMENT ON TABLE ppc_proddisclosureftrpln_l IS 'Association table for ProdDisclosureFeaturePlan';

CREATE INDEX ppc_17970beb ON ppc_proddisclosureftrpln_l(attributename_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_745400 ON ppc_proddisclosureftrpln_l(orderindex_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_d25fe935 ON ppc_proddisclosureftrpln_l(referencemodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_ba0fccd4 ON ppc_proddisclosureftrpln_l(referencemodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_556cf8 ON ppc_proddisclosureftrpln_l(referencedbymodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_40b36bd7 ON ppc_proddisclosureftrpln_l(referencedbymodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_e0628d82 ON ppc_proddisclosureftrpln_l(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_e0685167 ON ppc_proddisclosureftrpln_l(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_b4a89647 ON ppc_proddisclosureftrpln_l(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_proddisclosureftrplnrcrd (enableflag_ numeric(1) DEFAULT 0, data_text_ VARCHAR(100), data_number_ numeric(3, 2) DEFAULT 0, id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_eabd84e5" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

CREATE INDEX ppc_8db2100c ON ppc_proddisclosureftrplnrcrd(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_8db7d3f1 ON ppc_proddisclosureftrplnrcrd(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_a1799a7d ON ppc_proddisclosureftrplnrcrd(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_proddsclsrftrplnrcrd_l (attributename_ VARCHAR(64), orderindex_ numeric(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_fa56aae" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

COMMENT ON TABLE ppc_proddsclsrftrplnrcrd_l IS 'Association table for ProdDisclosureFeaturePlanRecord';

CREATE INDEX ppc_e23b2c78 ON ppc_proddsclsrftrplnrcrd_l(attributename_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_8c45c693 ON ppc_proddsclsrftrplnrcrd_l(orderindex_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_6407e188 ON ppc_proddsclsrftrplnrcrd_l(referencemodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_819afc67 ON ppc_proddsclsrftrplnrcrd_l(referencemodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_2a302fc5 ON ppc_proddsclsrftrplnrcrd_l(referencedbymodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_5ee8af64 ON ppc_proddsclsrftrplnrcrd_l(referencedbymodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_b8962a55 ON ppc_proddsclsrftrplnrcrd_l(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_b89bee3a ON ppc_proddsclsrftrplnrcrd_l(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_5f49314 ON ppc_proddsclsrftrplnrcrd_l(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_proddisclosureftrrgnrcrd (enableflag_ numeric(1) DEFAULT 0, data_text_ VARCHAR(100), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_edfa3c1e" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

CREATE INDEX ppc_a8de82c5 ON ppc_proddisclosureftrrgnrcrd(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_a8e446aa ON ppc_proddisclosureftrrgnrcrd(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_58076ca4 ON ppc_proddisclosureftrrgnrcrd(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_proddsclsrftrrgnrcrd_l (attributename_ VARCHAR(64), orderindex_ numeric(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_369137a7" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

COMMENT ON TABLE ppc_proddsclsrftrrgnrcrd_l IS 'Association table for ProdDisclosureFeatureRegionRecord';

CREATE INDEX ppc_fe93fc1f ON ppc_proddsclsrftrrgnrcrd_l(attributename_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_5a54704c ON ppc_proddsclsrftrrgnrcrd_l(orderindex_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_27149c81 ON ppc_proddsclsrftrrgnrcrd_l(referencemodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_b464dd20 ON ppc_proddsclsrftrrgnrcrd_l(referencemodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_50a2662c ON ppc_proddsclsrftrrgnrcrd_l(referencedbymodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_b1a6e80b ON ppc_proddsclsrftrrgnrcrd_l(referencedbymodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_ba70d2ce ON ppc_proddsclsrftrrgnrcrd_l(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_ba7696b3 ON ppc_proddsclsrftrrgnrcrd_l(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_5056777b ON ppc_proddsclsrftrrgnrcrd_l(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_proddisclosurefeeplan (id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_99e05d6f" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

CREATE INDEX ppc_579a5bd5 ON ppc_proddisclosurefeeplan(branchid_, temporalmodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_579a418e ON ppc_proddisclosurefeeplan(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_57a00573 ON ppc_proddisclosurefeeplan(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_44e430bb ON ppc_proddisclosurefeeplan(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_proddisclosurefeepln_l (attributename_ VARCHAR(64), orderindex_ numeric(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_b78d287d" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

COMMENT ON TABLE ppc_proddisclosurefeepln_l IS 'Association table for ProdDisclosureFeePlan';

CREATE INDEX ppc_e9e49709 ON ppc_proddisclosurefeepln_l(attributename_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_fc51cda2 ON ppc_proddisclosurefeepln_l(orderindex_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_302157 ON ppc_proddisclosurefeepln_l(referencemodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_b4b28476 ON ppc_proddisclosurefeepln_l(referencemodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_5a09a996 ON ppc_proddisclosurefeepln_l(referencedbymodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_fe4af8f5 ON ppc_proddisclosurefeepln_l(referencedbymodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_87bc68a4 ON ppc_proddisclosurefeepln_l(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_87c22c89 ON ppc_proddisclosurefeepln_l(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_56f5cee5 ON ppc_proddisclosurefeepln_l(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_proddisclosurefeeplnrecord (enableflag_ numeric(1) DEFAULT 0, data_text_ VARCHAR(100), data_number_ numeric(3, 2) DEFAULT 0, id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_a2ebe599" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

CREATE INDEX ppc_e6bef5c0 ON ppc_proddisclosurefeeplnrecord(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_e6c4b9a5 ON ppc_proddisclosurefeeplnrecord(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_bd1f2e49 ON ppc_proddisclosurefeeplnrecord(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_proddsclsrfplnrcrd_l (attributename_ VARCHAR(64), orderindex_ numeric(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_1349df30" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

COMMENT ON TABLE ppc_proddsclsrfplnrcrd_l IS 'Association table for ProdDisclosureFeePlanRecord';

CREATE INDEX ppc_9d523a36 ON ppc_proddsclsrfplnrcrd_l(attributename_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_2c69c195 ON ppc_proddsclsrfplnrcrd_l(orderindex_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_43d0130a ON ppc_proddsclsrfplnrcrd_l(referencemodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_901cd569 ON ppc_proddsclsrfplnrcrd_l(referencemodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_ebe97703 ON ppc_proddsclsrfplnrcrd_l(referencedbymodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_976d1f22 ON ppc_proddsclsrfplnrcrd_l(referencedbymodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_560b0ed7 ON ppc_proddsclsrfplnrcrd_l(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_5610d2bc ON ppc_proddsclsrfplnrcrd_l(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_a7f01652 ON ppc_proddsclsrfplnrcrd_l(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_proddisclosurefeergnrcrd (enableflag_ numeric(1) DEFAULT 0, data_text_ VARCHAR(100), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_eaf477c0" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

CREATE INDEX ppc_e12e1d67 ON ppc_proddisclosurefeergnrcrd(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_e133e14c ON ppc_proddisclosurefeergnrcrd(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_9be8f5c2 ON ppc_proddisclosurefeergnrcrd(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_proddsclsrfrgnrcrd_l (attributename_ VARCHAR(64), orderindex_ numeric(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_3a35ac29" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

COMMENT ON TABLE ppc_proddsclsrfrgnrcrd_l IS 'Association table for ProdDisclosureFeeRegionRecord';

CREATE INDEX ppc_b9ab09dd ON ppc_proddsclsrfrgnrcrd_l(attributename_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_fa786b4e ON ppc_proddsclsrfrgnrcrd_l(orderindex_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_6dcce03 ON ppc_proddsclsrfrgnrcrd_l(referencemodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_c2e6b622 ON ppc_proddsclsrfrgnrcrd_l(referencemodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_125bad6a ON ppc_proddsclsrfrgnrcrd_l(referencedbymodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_ea2b57c9 ON ppc_proddsclsrfrgnrcrd_l(referencedbymodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_57e5b750 ON ppc_proddsclsrfrgnrcrd_l(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_57eb7b35 ON ppc_proddsclsrfrgnrcrd_l(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_f251fab9 ON ppc_proddsclsrfrgnrcrd_l(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_proddisclosureformplan (id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_628e03a9" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

CREATE INDEX ppc_c343e817 ON ppc_proddisclosureformplan(branchid_, temporalmodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_c343cdd0 ON ppc_proddisclosureformplan(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_c34991b5 ON ppc_proddisclosureformplan(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_2f971439 ON ppc_proddisclosureformplan(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_proddisclosureformpln_l (attributename_ VARCHAR(64), orderindex_ numeric(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT PK_4944983 PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

COMMENT ON TABLE ppc_proddisclosureformpln_l IS 'Association table for ProdDisclosureFormPlan';

CREATE INDEX ppc_b0538cb ON ppc_proddisclosureformpln_l(attributename_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_bd205320 ON ppc_proddisclosureformpln_l(orderindex_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_14981055 ON ppc_proddisclosureformpln_l(referencemodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_4edaabf4 ON ppc_proddisclosureformpln_l(referencemodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_4e671d8 ON ppc_proddisclosureformpln_l(referencedbymodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_6516b8b7 ON ppc_proddisclosureformpln_l(referencedbymodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_914464a2 ON ppc_proddisclosureformpln_l(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_914a2887 ON ppc_proddisclosureformpln_l(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_c29f5b27 ON ppc_proddisclosureformpln_l(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_proddisclosureformplanrcrd (printing_option_ VARCHAR(256), enableflag_ numeric(1) DEFAULT 0, data_text_ VARCHAR(100), data_number_ numeric(3, 2) DEFAULT 0, id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_b98a9926" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

CREATE INDEX ppc_608f1acd ON ppc_proddisclosureformplanrcrd(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_6094deb2 ON ppc_proddisclosureformplanrcrd(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_b5fb39c ON ppc_proddisclosureformplanrcrd(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_proddsclsrfrmplnrcrd_l (attributename_ VARCHAR(64), orderindex_ numeric(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_a35ab6eb" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

COMMENT ON TABLE ppc_proddsclsrfrmplnrcrd_l IS 'Association table for ProdDisclosureFormPlanRecord';

CREATE INDEX ppc_665a6b5b ON ppc_proddsclsrfrmplnrcrd_l(attributename_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_214aaa90 ON ppc_proddsclsrfrmplnrcrd_l(orderindex_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_941f13c5 ON ppc_proddsclsrfrmplnrcrd_l(referencemodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_8ae9364 ON ppc_proddsclsrfrmplnrcrd_l(referencemodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_858f7868 ON ppc_proddsclsrfrmplnrcrd_l(referencedbymodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_5f985b47 ON ppc_proddsclsrfrmplnrcrd_l(referencedbymodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_9c211012 ON ppc_proddsclsrfrmplnrcrd_l(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_9c26d3f7 ON ppc_proddsclsrfrmplnrcrd_l(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_fcc781b7 ON ppc_proddsclsrfrmplnrcrd_l(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_proddisclosurewaiverrecord (enableflag_ numeric(1) DEFAULT 0, data_text_ VARCHAR(100), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_ba21a8e1" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

CREATE INDEX ppc_1b51d508 ON ppc_proddisclosurewaiverrecord(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_1b5798ed ON ppc_proddisclosurewaiverrecord(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_b68f4601 ON ppc_proddisclosurewaiverrecord(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_proddisclosurewvrrcrd_l (attributename_ VARCHAR(64), orderindex_ numeric(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_64dcf91b" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

COMMENT ON TABLE ppc_proddisclosurewvrrcrd_l IS 'Association table for ProdDisclosureWaiverRecord';

CREATE INDEX ppc_1840a033 ON ppc_proddisclosurewvrrcrd_l(attributename_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_216464b8 ON ppc_proddisclosurewvrrcrd_l(orderindex_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_e87e8fed ON ppc_proddisclosurewvrrcrd_l(referencemodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_c31fa58c ON ppc_proddisclosurewvrrcrd_l(referencemodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_1940ab40 ON ppc_proddisclosurewvrrcrd_l(referencedbymodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_cbc8381f ON ppc_proddisclosurewvrrcrd_l(referencedbymodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_37fb883a ON ppc_proddisclosurewvrrcrd_l(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_38014c1f ON ppc_proddisclosurewvrrcrd_l(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_6d29648f ON ppc_proddisclosurewvrrcrd_l(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_proddisclosurewvrrgnrcrd (enableflag_ numeric(1) DEFAULT 0, data_text_ VARCHAR(100), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_d410f56d" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

CREATE INDEX ppc_c749fc94 ON ppc_proddisclosurewvrrgnrcrd(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_c74fc079 ON ppc_proddisclosurewvrrgnrcrd(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_66dfcf5 ON ppc_proddisclosurewvrrgnrcrd(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_proddsclsrwvrrgnrcrd_l (attributename_ VARCHAR(64), orderindex_ numeric(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_f1ded936" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

COMMENT ON TABLE ppc_proddsclsrwvrrgnrcrd_l IS 'Association table for ProdDisclosureWaiverRegionRecord';

CREATE INDEX ppc_6f179af0 ON ppc_proddsclsrwvrrgnrcrd_l(attributename_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_198cab1b ON ppc_proddsclsrwvrrgnrcrd_l(orderindex_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_8be34010 ON ppc_proddsclsrwvrrgnrcrd_l(referencemodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_2018d8ef ON ppc_proddsclsrwvrrgnrcrd_l(referencemodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_5b6de43d ON ppc_proddsclsrwvrrgnrcrd_l(referencedbymodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_378b25dc ON ppc_proddsclsrwvrrgnrcrd_l(referencedbymodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_ebe514dd ON ppc_proddsclsrwvrrgnrcrd_l(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_ebead8c2 ON ppc_proddsclsrwvrrgnrcrd_l(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_ff5a378c ON ppc_proddsclsrwvrrgnrcrd_l(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_prodorganizationalstrctr (id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), description_ VARCHAR(1024), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), createdby_ VARCHAR(64), modifiedby_ VARCHAR(64), branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_3cdf0355" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

CREATE INDEX ppc_dd0ef0c3 ON ppc_prodorganizationalstrctr(branchid_, temporalmodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_dd0ed67c ON ppc_prodorganizationalstrctr(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_dd149a61 ON ppc_prodorganizationalstrctr(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_4932460d ON ppc_prodorganizationalstrctr(revision_) TABLESPACE :INDEXSPACE;

CREATE TABLE ppc_prodorgnztnlstrctr_l (attributename_ VARCHAR(64), orderindex_ numeric(24) DEFAULT 0, referencemodelid_ VARCHAR(64), referencemodeltype_ VARCHAR(256), referencedbymodelid_ VARCHAR(64), referencedbymodeltype_ VARCHAR(256), id_ VARCHAR(64) NOT NULL, name_ VARCHAR(256), i18nname_ VARCHAR(64), i18ndescription_ VARCHAR(64), description_ VARCHAR(1024), createdby_ VARCHAR(64), createdon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, modifiedby_ VARCHAR(64), modifiedon_ TIMESTAMP WITHOUT TIME ZONE NOT NULL, branchid_ VARCHAR(64) NOT NULL, repositoryid_ VARCHAR(64), active_ numeric(1) DEFAULT 0, revision_ numeric(24) DEFAULT 0 NOT NULL, parentrevision_ numeric(24) DEFAULT 0, concretemodeltype_ VARCHAR(256), businessdatefrom_ TIMESTAMP WITHOUT TIME ZONE, businessdateto_ TIMESTAMP WITHOUT TIME ZONE, temporalmodelid_ VARCHAR(64), templateid_ VARCHAR(64), modelid_ VARCHAR(64), modeltype_ VARCHAR(256), CONSTRAINT "PK_68c56e4" PRIMARY KEY (id_)) TABLESPACE :TABLESPACE;

COMMENT ON TABLE ppc_prodorgnztnlstrctr_l IS 'Association table for ProdOrganizationalStructure';

CREATE INDEX ppc_6dfae702 ON ppc_prodorgnztnlstrctr_l(attributename_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_75112849 ON ppc_prodorgnztnlstrctr_l(orderindex_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_20b4a2be ON ppc_prodorgnztnlstrctr_l(referencemodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_c61c481d ON ppc_prodorgnztnlstrctr_l(referencemodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_75d85acf ON ppc_prodorgnztnlstrctr_l(referencedbymodelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_61323fee ON ppc_prodorgnztnlstrctr_l(referencedbymodeltype_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_6f228c8b ON ppc_prodorgnztnlstrctr_l(branchid_, modelid_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_6f285070 ON ppc_prodorgnztnlstrctr_l(name_) TABLESPACE :INDEXSPACE;

CREATE INDEX ppc_bd8121e ON ppc_prodorgnztnlstrctr_l(revision_) TABLESPACE :INDEXSPACE;

INSERT INTO core_databasechangelog (tag_, author_, md5sum_, modulename_, id_, modelid_, modeltype_) VALUES ('0.0.1', 'system', 'd72a105eb48c03bb5992b5cfaabe92cf', 'ppc', 'e393bd1b-bc22-434a-9175-4f26f31d267d', 'd1f42145-453a-43d3-ac07-17522bbc5b19', 'com.zafin.zplatform.models.DatabaseChangelog');

