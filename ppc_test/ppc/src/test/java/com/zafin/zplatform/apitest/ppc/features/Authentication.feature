Feature: Authentication Regression Tests

  # TS-2: Authentication
  # TC-1 Generate Token for MAKER_ROLE user
  @OracleTest @PostgresTest @auth @auth1
  Scenario: Generate tokens for MAKER_ROLE user
    Given The user is maker

  # TC-2 Generate Token for CHECKER_ROLE user
  @OracleTest @PostgresTest @auth
  Scenario: Generate tokens for CHECKER_ROLE user
    Given The user is checker
    Given The user is checker2

  # TC-3 Generate Token for MASTER_ROLE user
  @OracleTest @PostgresTest @auth
  Scenario: Generate tokens for MASTER_ROLE user
    Given The user is master

  # TC-4 Generate Token for ADMIN_ROLE user
  @OracleTest @PostgresTest @auth
  Scenario: Generate tokens for ADMIN_ROLE user
    Given The user is admin

