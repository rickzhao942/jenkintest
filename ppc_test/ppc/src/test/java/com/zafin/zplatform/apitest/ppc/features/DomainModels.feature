Feature: Domain Models Regression Tests

  # TC-1 Create domain model - BankProduct
  @OracleTest @PostgresTest @domainModels
  Scenario Outline: <testCaseName> Create a independent model - Country and workflow
    Given Create a new branch
    When Create model instances of "<modelNameB>" and parse payload from "<jsonName>"
    And Sumbit the workflow for approval
    And Claim a branch in review
    And Approve the workflow    
    And Get the branch after approval
    Given Create a new branch
    When Delete a model instance of "<modelNameB>"
    And Sumbit the workflow for approval
    And Claim a branch in review
    And Approve the workflow
    And Get the branch after approval


    Examples: 
      | testCaseName         | modelNameB     | jsonName           |
      | DomainModelTest TC-1 | Country        | Country.json       |
