package com.zafin.zplatform.apitest.ppc.publish;

import static io.restassured.RestAssured.given;

import java.io.File;

import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zafin.zplatform.apitest.ppc.constant.Endpoint;
import com.zafin.zplatform.apitest.ppc.stepDefinations.CoreWorkflow;

import io.restassured.response.Response;

public class Zar {
    private static final Logger LOGGER = LoggerFactory.getLogger(Zar.class);
    private static String fileSeparator = File.separator;

    public static String publish(String masterToken) {

        File directory = new File(".." + fileSeparator);
        LOGGER.debug(directory.getAbsolutePath());
        File file = new File(directory + fileSeparator + "mmtest3" +
                fileSeparator + "target" +
                fileSeparator + "mmtest3-1.0.0.zar");
        Response response = given().header(Endpoint.HEADER_AUTHORIZATION, masterToken)
                                   .contentType("multipart/form-data")
                                   .multiPart("zar", file)
                                   .when()
                                   .post(Endpoint.PUBLISH_ZAR_URI)
                                   .then()
                                   .log()
                                   .all()
                                   .statusCode(Endpoint.OK_200)
                                   .extract()
                                   .response();
        String zmoduleId = response.jsonPath().get("data[0].Id");
        LOGGER.debug("Zmodule_Id :" + zmoduleId);

        int datacount = response.jsonPath().get("dataCount");
        Assert.assertEquals("The data count is", 1, datacount);
        String dataType = response.jsonPath().get("dataType");
        Assert.assertEquals("The datatype is", "models.ZModuleRepository", dataType);
        return zmoduleId;

    }

    public static String getPublishStatus(String zmoduleId) {

        Response response = given().header(Endpoint.HEADER_AUTHORIZATION, CoreWorkflow.masterToken)
                                   .contentType(Endpoint.HEADER_CONTENT_TYPE)
                                   .when()
                                   .get(Endpoint.ZAR_STATUS_URI + zmoduleId)
                                   .then()
                                   .log()
                                   .all()
                                   .statusCode(Endpoint.OK_200)
                                   .extract()
                                   .response();

        String status = response.jsonPath().get("data[0].status");
        return status;
    }

    public static String loopZarStatus(String zmoduleId, String status) throws InterruptedException {
        int trial = 0;
        while (!status.equals(Endpoint.ZAR_LOADED) && !status.equals(Endpoint.ZAR_FAILED)
                && trial < Endpoint.TRIAL_MAXIMUM) {

            LOGGER.debug("The status is still " + status + ". Will get status from server again!");
            status = getPublishStatus(zmoduleId);
            LOGGER.debug("The status now is: " + status);
            Thread.sleep(Endpoint.INTERVAL);
            trial++;
        }
        return status;

    }
}
