package com.zafin.zplatform.apitest.ppc.util;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class Rand {
    public enum EnumBank {
        SCOTIA,
        RBC,
        TD,
        CIBC,
        BMO
    }

    public static String randomString() {

        final String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rand = new Random();
        while (salt.length() < 10) {
            int index = (int) (rand.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }

    public static int randomNumber() {
        Random rand = new Random();
        int randNumber = rand.nextInt(10000000);
        return randNumber;
    }

    public static float randomFloat() {
        Random rand = new Random();
        double randFloat = rand.nextFloat() + rand.nextInt(1000);
        DecimalFormat df = new DecimalFormat("###.##");
        // float floatValue = (float) (100.23 + rand.nextInt(100));
        return Float.valueOf(df.format(randFloat));
        // return floatValue;
    }

    public static BigDecimal randomDecimal() {
        BigDecimal min = new BigDecimal(1.01).setScale(2, BigDecimal.ROUND_HALF_UP);
        BigDecimal max = new BigDecimal(999.99).setScale(2, BigDecimal.ROUND_HALF_UP);
        BigDecimal randomBigDecimal = min.add(new BigDecimal(Math.random()).multiply(max.subtract(min)));
        return randomBigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP);

    }

    public static int randYearBetween(int start, int end) {
        int year = start + (int) Math.round(Math.random() * (end - start));
        return year;
    }

    public static String getCurrentDate() {
        final String DATE_FORMAT = "yyyy-MM-dd";
        final String TIME_FORMAT = "HH:mm:ss.SSS";
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
        SimpleDateFormat timeFormat = new SimpleDateFormat(TIME_FORMAT);
        String randomDate = dateFormat.format(date) + "T" + timeFormat.format(date) + "Z";
        System.out.println(randomDate);
        return randomDate;
    }

    @SuppressWarnings("static-access")
    public static String randomDate() {
        GregorianCalendar gc = new GregorianCalendar();
        int year = randYearBetween(2002, 2052);
        gc.set(gc.YEAR, year);
        int dayOfYear = randYearBetween(1, gc.getActualMaximum(gc.DAY_OF_YEAR));
        gc.set(gc.DAY_OF_YEAR, dayOfYear);
        String randomDate = String.valueOf(gc.get(gc.YEAR)) + "-" +
                (gc.get(gc.MONTH) + 1) + "-" +
                gc.get(gc.DAY_OF_MONTH);
        System.out.println(randomDate);
        return randomDate;

    }

    public static String randomTimestamp() {

        long offset = Timestamp.valueOf("2002-01-01 00:00:00").getTime();
        long end = Timestamp.valueOf("2052-01-01 00:00:00").getTime();
        long diff = end - offset + 1;
        Timestamp rand = new Timestamp(offset + (long) (Math.random() * diff));

        final String DATE_FORMAT = "yyyy-MM-dd";
        final String TIME_FORMAT = "HH:mm:ss";
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
        SimpleDateFormat timeFormat = new SimpleDateFormat(TIME_FORMAT);
        String randomTimestamp = dateFormat.format(rand) + "T" + timeFormat.format(rand) + "Z";
        return randomTimestamp;
    }

    public static boolean randomBoolean() {
        Random rand = new Random();
        boolean randBoolean = rand.nextBoolean();
        return randBoolean;
    }

    public static String randomEnum() {
        EnumBank[] VALUES = EnumBank.values();
        int SIZE = VALUES.length;
        Random RANDOM = new Random();
        String enumToString = VALUES[RANDOM.nextInt(SIZE)].toString();
        return enumToString;
    }

    @SuppressWarnings("unchecked")
    public static JSONObject randomI18nString(String langCode1, String langCode2, String langCode3) {
        JSONObject bodyJson = new JSONObject();
        bodyJson.put("i18nTexts", randomI18nArray(langCode1, langCode2, langCode3));
        return bodyJson;
    }

    @SuppressWarnings("unchecked")
    public static JSONObject randomI18nString(String langCode1, String langCode2) {
        JSONObject bodyJson = new JSONObject();
        bodyJson.put("i18nTexts", randomI18nArray(langCode1, langCode2));
        return bodyJson;
    }

    @SuppressWarnings("unchecked")
    public static JSONArray randomI18nArray(String langCode1, String langCode2, String langCode3) {
        JSONArray i18nArray = new JSONArray();
        i18nArray.add(randomI18nText(langCode1));
        i18nArray.add(randomI18nText(langCode2));
        i18nArray.add(randomI18nText(langCode3));
        return i18nArray;
    }

    @SuppressWarnings("unchecked")
    public static JSONArray randomI18nArray(String langCode1, String langCode2) {
        JSONArray i18nArray = new JSONArray();
        i18nArray.add(randomI18nText(langCode1));
        i18nArray.add(randomI18nText(langCode2));
        return i18nArray;
    }

    @SuppressWarnings("unchecked")
    public static JSONObject randomI18nText(String langCode) {
        JSONObject i18nText = new JSONObject();
        String text = randomString();
        i18nText.put("langCode", langCode);
        i18nText.put("text", text);
        return i18nText;
    }
}
