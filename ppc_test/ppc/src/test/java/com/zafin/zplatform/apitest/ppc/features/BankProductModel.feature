Feature: BankProduct Model Regression Tests

  # TC-1 Create domain model - BankProduct
  @PostgresTest @BankProductModel
  Scenario Outline: <testCaseName> Create a independent model - BankProduct and workflow
    Given Create a new branch
    When Create BankProduct model instance of "<modelName>" and parse payload from "<jsonName>"
    And Sumbit the workflow for approval
    And Claim a branch in review
    And Approve the workflow    
    And Get the branch after approval
    Given Create a new branch
    When Delete a BankProduct model instance of "<modelName>"
    And Sumbit the workflow for approval
    And Claim a branch in review
    And Approve the workflow
    And Get the branch after approval


    Examples: 
      | testCaseName              | modelName     | jsonName           |
      | BankProductModelTest TC-1 | BankProduct   | BankProduct.json   |
