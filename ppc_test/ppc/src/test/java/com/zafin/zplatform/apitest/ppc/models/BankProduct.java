package com.zafin.zplatform.apitest.ppc.models;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.HashMap;

import org.json.simple.JSONObject;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zafin.zplatform.apitest.ppc.constant.Endpoint;
//import com.zafin.zplatform.apitests.eventNotification.impl.models.mmtest.ModelB;
import com.zafin.zplatform.apitest.ppc.stepDefinations.CoreWorkflow;
import com.zafin.zplatform.apitest.ppc.util.Rand;

import io.restassured.response.Response;

public class BankProduct {
    private static final Logger LOGGER = LoggerFactory.getLogger(BankProduct.class);
    public static final String code = "code";
    public static Rand rand;
    public static final String name = "name";
    public static final String modelId = "modelId";
    public static final String href = "href";
    public static HashMap<String, String> modelMap;

    @SuppressWarnings({"unchecked", "static-access"})
    public static HashMap<String, String> create(String branchId, JSONObject bodyJson) throws Throwable {
        String makerToken = CoreWorkflow.makerToken;
        String url = Endpoint.BRANCH_URI + branchId + Endpoint.BANK_PRODUCT_URI;
        String nameExpectedValue = "BankProduct-" + rand.randomString();


        bodyJson.replace(BankProduct.name, nameExpectedValue);
        bodyJson.replace(BankProduct.code, "CRC");

        LOGGER.debug(bodyJson.toJSONString());

        Response response = given().auth().basic("maker", "maker")
                                   .contentType(Endpoint.HEADER_CONTENT_TYPE)
                                   .body(bodyJson.toJSONString())
                                   .when()
                                   .post(url)
                                   .then()
                                   .log()
                                   .all()
                                   .statusCode(Endpoint.OK_200)
                                   .extract()
                                   .response();
        String modelIdB = response.jsonPath().get("data[0].modelId");
        String hrefB = response.jsonPath().get("data[0]._links[0].href");
        String nameActualValue = response.jsonPath().get("data[0].name");

        Assert.assertNotNull(modelIdB);
        assertThat(nameActualValue, equalTo(nameExpectedValue));
        assertThat(nameActualValue, is(nameExpectedValue));
 
        Assert.assertEquals(Endpoint.ERROR_MESSAGE + name, nameExpectedValue, nameActualValue);


        LOGGER.info("****A BankProduct instance has been created!" + "\n" +
                "The model name is: " + nameActualValue + "\n" +
                "The modelId is: " + modelIdB);
        modelMap = new HashMap<String, String>();
        modelMap.put(name, nameActualValue);
        modelMap.put(modelId, modelIdB);
        modelMap.put(href, hrefB);
        return modelMap;

    }

    public static void delete(String branchId, String modelId) throws Throwable {
        String makerToken = CoreWorkflow.makerToken;
        String url = Endpoint.BRANCH_URI + branchId + Endpoint.BANK_PRODUCT_URI + "/" + modelId;
        Response response = given().auth().basic("maker", "maker")
                                   .contentType(Endpoint.HEADER_CONTENT_TYPE)
                                   .body("")
                                   .when()
                                   .delete(url)
                                   .then()
                                   .log()
                                   .all()
                                   .statusCode(Endpoint.OK_200)
                                   .extract()
                                   .response();

        int dataCount = response.jsonPath().get("dataCount");
        Assert.assertNotNull(response.jsonPath().get("requestDate"));
        Assert.assertEquals(Endpoint.ERROR_MESSAGE + "datacount", 0, dataCount);
    }
}
