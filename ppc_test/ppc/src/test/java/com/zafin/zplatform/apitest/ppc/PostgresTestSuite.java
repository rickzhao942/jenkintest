package com.zafin.zplatform.apitest.ppc;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.zafin.zplatform.apitest.ppc.runner.BankProductModelRunner;

@RunWith(Suite.class)
@SuiteClasses({
//		AuthenticationRunnerPostgres.class
    	//DomainModelsRunner.class
		BankProductModelRunner.class
})
public class PostgresTestSuite {

}
