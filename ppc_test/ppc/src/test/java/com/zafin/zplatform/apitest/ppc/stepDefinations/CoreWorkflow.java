package com.zafin.zplatform.apitest.ppc.stepDefinations;

import java.io.IOException;
import java.util.HashMap;

import org.json.simple.JSONObject;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zafin.zplatform.apitest.ppc.constant.Endpoint;
//import com.zafin.zplatform.apitests.eventNotification.validation.*;
import com.zafin.zplatform.apitest.ppc.core.Branch;
import com.zafin.zplatform.apitest.ppc.core.Workflow;
import com.zafin.zplatform.apitest.ppc.publish.PopulateImplTables;
import com.zafin.zplatform.apitest.ppc.publish.Token;
import com.zafin.zplatform.apitest.ppc.publish.Zar;
import com.zafin.zplatform.apitest.ppc.util.AssertTokens;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;

public class CoreWorkflow {
    private static String username;
    private static String password;
    public static String makerToken = "maker";
    public static String checkerToken;
    public static String masterToken;
    public static Response response;

    public static String branchId;
    public static String zmoduleId;

    public static HashMap<String, String> mapB1;
    public static HashMap<String, String> mapB2;
    public static HashMap<String, String> mapB3;
    public static HashMap<String, String> mapB4;
    public static HashMap<String, String> mapA1;

    public static final String destinationName = Endpoint.JMS_DESTINATION_NAME_MMTEST;
    public static final String destinationName1 = Endpoint.JMS_DESTINATION_NAME_MMTEST1;
    public static final String destinationName2 = Endpoint.JMS_DESTINATION_NAME_MMTEST2;
    public static final String destinationName3 = Endpoint.JMS_DESTINATION_NAME_MMTEST3;

    public static JSONObject bodyJson;
//    public static Rand rand;

    private static final Logger LOGGER = LoggerFactory.getLogger(CoreWorkflow.class);

    @Test
    @Given("^The user is maker$")

    // Generate tokens for users
    public static String getMakerToken() throws Throwable {
        username = "maker";
        password = "maker";
        response = Token.generate(username, password);
        makerToken = AssertTokens.assertMaker(response);
        LOGGER.info("**** Token of user " + username + " is: " + makerToken);
        return makerToken;
    }

    @Test
    @Given("^The user is checker$")
    public static String getCheckerToken() throws Throwable {
        username = "checker";
        password = "checker";
        response = Token.generate(username, password);
        checkerToken = AssertTokens.assertChecker(response);
        LOGGER.info("**** Token of user " + username + " is: " + checkerToken);
        return checkerToken;
    }

    @Test
    @Given("^The user is master$")
    public static String getMasterToken() throws Throwable {
        username = "master";
        password = "master";
        response = Token.generate(username, password);
        masterToken = AssertTokens.assertMaster(response);
        LOGGER.info("**** Token of user " + username + " is: " + masterToken);
        return masterToken;
    }

    @Test
    @Given("^The user is admin$")
    public void getAdminToken() throws Throwable {
        username = "admin";
        password = "admin";
        response = Token.generate(username, password);
        String adminToken = AssertTokens.assertAdmin(response);
        LOGGER.info("**** Token of user " + username + " is: " + adminToken);

    }

    @Test
    @Given("^The user is checker2$")
    public void getChecker2Token() throws Throwable {
        username = "checker2";
        password = "checker2";
        response = Token.generate(username, password);
        checkerToken = AssertTokens.assertChecker(response);
        LOGGER.info("**** Token of user " + username + " is: " + checkerToken);
    }

    // Drop all the tables and populate impl tables

    @Test
    @Given("^Sql files to clean oracle implDB$")
    public void cleanOracleDatabase() throws Throwable {

        PopulateImplTables.cleanOracleDatabase();
        LOGGER.info("****Oracle database is clean!");

    }

    @Test
    @When("^Connect to Oracle database and Execute the sql for implementation tables$")
    public void runImplScriptsOracle() throws IOException {

        PopulateImplTables.runImplScriptsOracle();
        LOGGER.info("****Oralce Impl tables have been populated!");

    }

    @Test
    @Given("^Sql files to clean postgres implDB$")
    public static void cleanPostgresDatabase() throws Throwable {

        PopulateImplTables.cleanPostgresDatabase();
        LOGGER.info("****Postgres database is clean!");

    }

    @Test
    @When("^Connect to postgres database and Execute the sql for implementation tables$")
    public static void runImplScriptsPostgres() throws IOException {

        PopulateImplTables.runImplScriptsPostgres();
        LOGGER.info("****Postgres Impl tables have been populated!");

    }

    // PUBLISH THE ZAR

    @Test
    @Given("^The server is up and running with core tables on the database$")
    public void serverIsUp() throws IOException {

        LOGGER.info("Server is up and running");

    }

    @Test
    @When("^Publish the zar$")
    public static String publishZar() throws IOException {
        zmoduleId = Zar.publish(masterToken);
        LOGGER.info("****Publishing the Zar file!");
        return zmoduleId;
    }

    @Test
    @Then("^Get status of the zar$")
    public static void getZarStatus() throws IOException, InterruptedException {
        String status = Zar.getPublishStatus(zmoduleId);
        LOGGER.info("****Zar initial status is: " + status);
        status = Zar.loopZarStatus(zmoduleId, status);
        LOGGER.info("Zar final status is: " + status);

    }

    // WORKFLOW ACTIONS
    @Test
    @Given("^Starting the workflow$")
    public void startWorkflow() throws IOException {
        LOGGER.info("The workflow actions are started");
    }

    @Test
    @Given("^Create a new branch$")
    public static String createBranch() throws Throwable {
//        TestJMSMessage.clearJMSMessage();
        String branchName = Branch.createBranchName();
        LOGGER.info("branchName=" + branchName);
        branchId = Branch.create(branchName);
        return branchId;
    }

    @Test
    @When("^Sumbit the workflow for approval$")
    public static void submitWorkflow() throws Throwable {

        Workflow.submit(branchId);

    }

    @Test
    @When("^Claim a branch in review$")
    public static void claimWorkflow() throws Throwable {

        Workflow.claim(branchId);

    }

    @Test
    @When("^Approve the workflow$")
    public static void approveWorkflow() throws Throwable {
        Workflow.approve(branchId);

    }

    @Test
    @Then("^Get the branch after approval$")
    public static void loopWorkflowAfterApproval() throws Throwable {
        String status = Branch.get(branchId);
        Workflow.loopWorkflowStatus(branchId, status);

    }

}
