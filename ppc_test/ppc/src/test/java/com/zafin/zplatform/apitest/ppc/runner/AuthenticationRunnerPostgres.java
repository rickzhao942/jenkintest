package com.zafin.zplatform.apitest.ppc.runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(monochrome = true,
        features = {"src/test/java/com/zafin/zplatform/apitest/ppc/features"},
        glue = "com.zafin.zplatform.apitest.ppc.stepDefinations",
        plugin = {"pretty", "html:target/cucumber-html-reports/Authentication"},
        tags = {"@auth,@pg-zar,@jms"})

public class AuthenticationRunnerPostgres {

}