package com.zafin.zplatform.apitest.ppc.publish;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.DriverManager;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zafin.zplatform.apitest.ppc.constant.Endpoint;

public class PopulateImplTables {
    private static final Logger LOGGER = LoggerFactory.getLogger(PopulateImplTables.class);
    private static String fileSeparator = File.separator;

    public static void cleanOracleDatabase() {

        String driver = Endpoint.ORACLE_DRIVER;
        String url = Endpoint.ORACLE_URL;
        String username = Endpoint.ORACLE_USERNAME;
        String pwd = Endpoint.ORACLE_PASSWORD;

        File directory = new File("." + fileSeparator);
        LOGGER.debug(directory.getAbsolutePath());
        File file = new File(directory + fileSeparator + "drop-impl-tables.sql");

        try {

            Class.forName(driver);
            new ScriptRunner(DriverManager.getConnection(url, username, pwd))
                                                                             .runScript(new BufferedReader(new FileReader(file)));
            LOGGER.debug("Connecting to oracle database");

        } catch (Exception e) {
            LOGGER.error(e.toString());
        }
    }

    public static void cleanPostgresDatabase() {

        String driver = Endpoint.POSTGRES_DRIVER;
        String url = Endpoint.POSTGRES_URL;
        String username = Endpoint.POSTGRES_USERNAME;
        String pwd = Endpoint.POSTGRES_PASSWORD;

        File directory = new File("." + fileSeparator);
        LOGGER.debug(directory.getAbsolutePath());
        File file = new File(directory + fileSeparator + "drop-impl-tables.sql");

        try {

            Class.forName(driver);
            new ScriptRunner(DriverManager.getConnection(url, username, pwd))
                                                                             .runScript(new BufferedReader(new FileReader(file)));
            LOGGER.debug("Connecting to postgres database");

        } catch (Exception e) {
            LOGGER.error(e.toString());
        }
    }

    public static void runImplScriptsOracle() {
        String driver = Endpoint.ORACLE_DRIVER;
        String url = Endpoint.ORACLE_URL;
        String username = Endpoint.ORACLE_USERNAME;
        String pwd = Endpoint.ORACLE_PASSWORD;

        File directory = new File(".." + fileSeparator);
        LOGGER.debug(directory.getAbsolutePath());

        File mmtestScript = new File(directory + fileSeparator + "mmtest" +
                fileSeparator + "resources" +
                fileSeparator + "sql" +
                fileSeparator + "mmtest-oracle-1.0.0.sql");
        File mmtest1Script = new File(directory + fileSeparator + "mmtest1" +
                fileSeparator + "resources" +
                fileSeparator + "sql" +
                fileSeparator + "mmtest1-oracle-1.0.0.sql");
        File mmtest2Script = new File(directory + fileSeparator + "mmtest2" +
                fileSeparator + "resources" +
                fileSeparator + "sql" +
                fileSeparator + "mmtest2-oracle-1.0.0.sql");
        File mmtest3Script = new File(directory + fileSeparator + "mmtest3" +
                fileSeparator + "resources" +
                fileSeparator + "sql" +
                fileSeparator + "mmtest3-oracle-1.0.0.sql");
        try {
            Class.forName(driver);
            LOGGER.debug("Connecting to oracle database");
            new ScriptRunner(DriverManager.getConnection(url, username, pwd))
                                                                             .runScript(new BufferedReader(new FileReader(mmtestScript)));
            new ScriptRunner(DriverManager.getConnection(url, username, pwd))
                                                                             .runScript(new BufferedReader(new FileReader(mmtest1Script)));
            new ScriptRunner(DriverManager.getConnection(url, username, pwd))
                                                                             .runScript(new BufferedReader(new FileReader(mmtest2Script)));
            new ScriptRunner(DriverManager.getConnection(url, username, pwd))
                                                                             .runScript(new BufferedReader(new FileReader(mmtest3Script)));
        } catch (Exception e) {
            LOGGER.error(e.toString());
        }

        LOGGER.debug(mmtestScript.toString());
        LOGGER.debug("connected to oracle");
        LOGGER.debug(driver);
        LOGGER.debug(url);

    }

    public static void runImplScriptsPostgres() {
        String driver = Endpoint.POSTGRES_DRIVER;
        String url = Endpoint.POSTGRES_URL;
        String username = Endpoint.POSTGRES_USERNAME;
        String pwd = Endpoint.POSTGRES_PASSWORD;

        File directory = new File(".." + fileSeparator);
        LOGGER.debug(directory.getAbsolutePath());

        File mmtestScript = new File(directory + fileSeparator + "mmtest" +
                fileSeparator + "resources" +
                fileSeparator + "sql" +
                fileSeparator + "mmtest-postgresql-1.0.0.sql");
        File mmtest1Script = new File(directory + fileSeparator + "mmtest1" +
                fileSeparator + "resources" +
                fileSeparator + "sql" +
                fileSeparator + "mmtest1-postgresql-1.0.0.sql");
        File mmtest2Script = new File(directory + fileSeparator + "mmtest2" +
                fileSeparator + "resources" +
                fileSeparator + "sql" +
                fileSeparator + "mmtest2-postgresql-1.0.0.sql");
        File mmtest3Script = new File(directory + fileSeparator + "mmtest3" +
                fileSeparator + "resources" +
                fileSeparator + "sql" +
                fileSeparator + "mmtest3-postgresql-1.0.0.sql");
        try {
            Class.forName(driver);
            LOGGER.debug("Connecting to postgres database");
            new ScriptRunner(DriverManager.getConnection(url, username, pwd))
                                                                             .runScript(new BufferedReader(new FileReader(mmtestScript)));
            new ScriptRunner(DriverManager.getConnection(url, username, pwd))
                                                                             .runScript(new BufferedReader(new FileReader(mmtest1Script)));
            new ScriptRunner(DriverManager.getConnection(url, username, pwd))
                                                                             .runScript(new BufferedReader(new FileReader(mmtest2Script)));
            new ScriptRunner(DriverManager.getConnection(url, username, pwd))
                                                                             .runScript(new BufferedReader(new FileReader(mmtest3Script)));
        } catch (Exception e) {
            LOGGER.error(e.toString());
        }

        LOGGER.debug(mmtestScript.toString());
        LOGGER.debug("connected to postgres");
        LOGGER.debug(driver);
        LOGGER.debug(url);

    }
}
