package com.zafin.zplatform.apitest.ppc.util;

import org.junit.Assert;

import io.restassured.response.Response;

public class AssertTokens {
    public static String assertMaker(Response response) {
        String tokenType = response.jsonPath().get("token_type");
        String accessToken = response.jsonPath().get("access_token");
        String role = response.jsonPath().get("roles");
        String makerToken = tokenType + " " + accessToken;

        Assert.assertNotNull(tokenType);
        Assert.assertNotNull(accessToken);
        Assert.assertEquals("bearer", tokenType);
        Assert.assertEquals("The role is", "MAKER_ROLEROLE->", role);
        return makerToken;
    }

    public static String assertChecker(Response response) {
        String tokenType = response.jsonPath().get("token_type");
        String accessToken = response.jsonPath().get("access_token");
        String role = response.jsonPath().get("roles");
        String checkerToken = tokenType + " " + accessToken;

        Assert.assertNotNull(tokenType);
        Assert.assertNotNull(accessToken);
        Assert.assertEquals("bearer", tokenType);
        Assert.assertEquals("The role is", "CHECKER_ROLEROLE->", role);
        return checkerToken;
    }

    public static String assertMaster(Response response) {
        String tokenType = response.jsonPath().get("token_type");
        String accessToken = response.jsonPath().get("access_token");
        String role = response.jsonPath().get("roles");
        String masterToken = tokenType + " " + accessToken;

        Assert.assertNotNull(tokenType);
        Assert.assertNotNull(accessToken);
        Assert.assertEquals("bearer", tokenType);
        Assert.assertEquals("The role is", "MASTER_WRITE_ROLEROLE->", role);
        return masterToken;

    }

    public static String assertAdmin(Response response) {
        String tokenType = response.jsonPath().get("token_type");
        String accessToken = response.jsonPath().get("access_token");
        String role = response.jsonPath().get("roles");
        String adminToken = tokenType + " " + accessToken;

        Assert.assertNotNull(tokenType);
        Assert.assertNotNull(accessToken);
        Assert.assertEquals("bearer", tokenType);
        Assert.assertEquals("The role is", "ADMIN_ROLEROLE->", role);
        return adminToken;
    }
}
