package com.zafin.zplatform.apitest.ppc.core;

import static io.restassured.RestAssured.given;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.simple.JSONObject;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zafin.zplatform.apitest.ppc.constant.Endpoint;

import io.restassured.response.Response;

public class Branch {
    //private static final Logger LOGGER = Logger.getLogger(Branch.class);

	final static Logger LOGGER = LoggerFactory.getLogger(Branch.class);

    
    public static String createBranchName() {
        String dateFormat = "MM/dd/yyyy HH:mm:ss.SSS";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);

        String branchName = "maker_" + simpleDateFormat.format(new Date());
        LOGGER.debug("Branch name is: " + branchName);
        return branchName;
    }

    @SuppressWarnings("unchecked")
    public static String create(String branchName) {

        JSONObject bodyJson = new JSONObject();
        bodyJson.put("name", branchName);

        Response response = given().auth().basic("maker", "maker")
                                   .contentType(Endpoint.HEADER_CONTENT_TYPE)
                                   .body(bodyJson.toJSONString())
                                   .when()
                                   .post(Endpoint.BRANCH_URI)
                                   .then()
                                   .log()
                                   .all()
                                   .statusCode(Endpoint.OK_200)
                                   .extract()
                                   .response();

        String branchId = response.jsonPath().get("data[0].branchId");
        String dataType = response.jsonPath().get("dataType");
        String branchStatus = response.jsonPath().get("data[0].branchStatus");
        String workflowStatus = response.jsonPath().get("data[0].workflowStatus");
        Assert.assertNotNull(branchId);
        Assert.assertEquals("The datatype is", "RsRevisionBranch", dataType);
        Assert.assertEquals("The branchStatus is ", Endpoint.BRANCH_DRAFT, branchStatus);
        Assert.assertEquals("The workflowStatus is", Endpoint.WORKFLOW_DRAFT, workflowStatus);

        LOGGER.debug("****Created a new branch. The branchId is: " + branchId);
        return branchId;
    }

    public static String get(String branchId) {
        Response response = given().auth().basic("maker", "maker")
                                   .contentType(Endpoint.HEADER_CONTENT_TYPE)
                                   .when()
                                   .get(Endpoint.BRANCH_URI + branchId)
                                   .then()
                                   .log()
                                   .all()
                                   .statusCode(Endpoint.OK_200)
                                   .extract()
                                   .response();
        String status = response.jsonPath().get("data[0].workflowStatus");
        LOGGER.debug("****Get a branch now! The workflow status is: " + status);
        return status;

    }

}
