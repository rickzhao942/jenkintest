package com.zafin.zplatform.apitest.ppc.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JsonParser {
    private static final Logger LOGGER = LoggerFactory.getLogger(JsonParser.class);
    private static String fileSeparator = File.separator;

    public static JSONObject jsonObject;

    public static JSONObject parse(String fileName) {

        JSONParser parser = new JSONParser();

        try {
            File directory = new File("." + fileSeparator);
            LOGGER.debug(directory.getAbsolutePath());
            String filePath = directory + fileSeparator + "json-payload" + fileSeparator + fileName;
            File file = new File(filePath);
            LOGGER.debug(filePath);

            Object obj = parser.parse(new FileReader(file));

            jsonObject = (JSONObject) obj;
            System.out.println(jsonObject);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

}