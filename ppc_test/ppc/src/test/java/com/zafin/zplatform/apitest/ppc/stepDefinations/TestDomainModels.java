package com.zafin.zplatform.apitest.ppc.stepDefinations;

import java.util.HashMap;

import org.json.simple.JSONObject;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zafin.zplatform.apitest.ppc.models.Country;
import com.zafin.zplatform.apitest.ppc.util.JsonParser;

import cucumber.api.java.en.When;

public class TestDomainModels {

    public static HashMap<String, String> map1;
    public static HashMap<String, String> map2;
    public static HashMap<String, String> map3;
    public static HashMap<String, String> map4;
    public static String modelIdB1 = null;

    public static JSONObject bodyJson;
    private static final Logger LOGGER = LoggerFactory.getLogger(TestDomainModels.class);

    @Test
    @When("^Create model instances of \"([^\"]*)\" and parse payload from \"([^\"]*)\"$")
    public static void create(String modelName, String jsonName) throws Throwable {
        bodyJson = JsonParser.parse(jsonName);
        String branchId = CoreWorkflow.branchId;
        map1 = Country.create(branchId, bodyJson);
        LOGGER.info("Model instances of " + modelName + " have been created!");

    }

    @Test
    @When("^Delete a model instance of \"([^\"]*)\"$")
    public static void delete(String modelName) throws Throwable {
        String branchId = CoreWorkflow.branchId;
        String modelId = map1.get(Country.modelId);
        Country.delete(branchId, modelId);

        LOGGER.debug("Model instances of " + modelName + " have been created!");

    }
}
