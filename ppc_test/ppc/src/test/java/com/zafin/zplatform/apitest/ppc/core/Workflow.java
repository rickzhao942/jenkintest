package com.zafin.zplatform.apitest.ppc.core;

import static io.restassured.RestAssured.given;

import org.json.simple.JSONObject;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zafin.zplatform.apitest.ppc.constant.Endpoint;

import io.restassured.response.Response;

public class Workflow {
    private static final Logger LOGGER = LoggerFactory.getLogger(Workflow.class);

    @SuppressWarnings("unchecked")
    public static void submit(String branchId) {

        JSONObject bodyJson = new JSONObject();
        bodyJson.put("comment", "Submitted");

        Response response = given().auth().basic("maker", "maker")
                                   .contentType(Endpoint.HEADER_CONTENT_TYPE)
                                   .body(bodyJson.toJSONString())
                                   .when()
                                   .post(Endpoint.BRANCH_URI + branchId + Endpoint.SUBMIT_WORKFLOW)
                                   .then()
                                   .log()
                                   .all()
                                   .statusCode(Endpoint.OK_200)
                                   .extract()
                                   .response();
        String dataType = response.jsonPath().get("dataType");
        String branchStatus = Endpoint.BRANCH_UNCLAIMED;
        String workflowStatus = Endpoint.WORKFLOW_REVIEW;
        String actualBranchStatus = response.jsonPath().get("data[0].branchStatus");
        String actualWorkflowStatus = response.jsonPath().get("data[0].workflowStatus");

        Assert.assertEquals("Datetype should be : ", "RsRevisionBranch", dataType);
        Assert.assertEquals("The branchStatus should be : ", branchStatus, actualBranchStatus);
        Assert.assertEquals("The workflowStatus should be : ", workflowStatus, actualWorkflowStatus);

        LOGGER.debug("****Submit to the workflow! The status is : " + actualWorkflowStatus);

    }

    public static void claim(String branchId) {

        JSONObject bodyJson = new JSONObject();
        Response response = given().auth().basic("checker", "checker")
                                   .contentType(Endpoint.HEADER_CONTENT_TYPE)
                                   .body(bodyJson.toJSONString())
                                   .when()
                                   .put(Endpoint.BRANCH_URI + branchId + Endpoint.CLAIM_WORKFLOW)
                                   .then()
                                   .log()
                                   .all()
                                   .statusCode(Endpoint.OK_200)
                                   .extract()
                                   .response();

        String dataType = response.jsonPath().get("dataType");
        String branchStatus = Endpoint.BRANCH_CLAIMED;
        String workflowStatus = Endpoint.WORKFLOW_REVIEW;
        String actualBranchStatus = response.jsonPath().get("data[0].branchStatus");
        String actualWorkflowStatus = response.jsonPath().get("data[0].workflowStatus");

        Assert.assertEquals("Datetype should be : ", "RsRevisionBranch", dataType);
        Assert.assertEquals("The branchStatus should be : ", branchStatus, actualBranchStatus);
        Assert.assertEquals("The workflowStatus should be : ", workflowStatus, actualWorkflowStatus);
        LOGGER.debug("****Claimed the workflow! The status is: " + actualWorkflowStatus);

    }

    @SuppressWarnings("unchecked")
    public static String approve(String branchId) {

        JSONObject bodyJson = new JSONObject();
        bodyJson.put("comment", "Approved");

        Response response = given().auth().basic("checker", "checker")
                                   .contentType(Endpoint.HEADER_CONTENT_TYPE)
                                   .body(bodyJson.toJSONString())
                                   .when()
                                   .post(Endpoint.BRANCH_URI + branchId + Endpoint.APPROVE_WORKFLOW)
                                   .then()
                                   .log()
                                   .all()
                                   .statusCode(Endpoint.OK_200)
                                   .extract()
                                   .response();
        String dataType = response.jsonPath().get("dataType");
        String branchStatus = Endpoint.BRANCH_MERGING;
        String workflowStatus = Endpoint.WORKFLOW_MERGE_QUEUED;
        String actualBranchStatus = response.jsonPath().get("data[0].branchStatus");
        String actualWorkflowStatus = response.jsonPath().get("data[0].workflowStatus");

        Assert.assertEquals("Datetype should be : ", "RsRevisionBranch", dataType);
        Assert.assertEquals("The branchStatus should be : ", branchStatus, actualBranchStatus);
        Assert.assertEquals("The workflowStatus should be : ", workflowStatus, actualWorkflowStatus);

        LOGGER.debug("****Approve the workflow! The status now is: " + workflowStatus);
        return workflowStatus;
    }

    public static void loopWorkflowStatus(String branchId, String status) throws InterruptedException {

        int trial = 0;
        while (!status.equals(Endpoint.WORKFLOW_COMPLETE) && trial < Endpoint.TRIAL_MAXIMUM) {

            LOGGER.debug("The status is still " + status + ". Will get status from server again!");

            status = Branch.get(branchId);
            LOGGER.debug("The status now is: " + status);
            Thread.sleep(Endpoint.INTERVAL);
            trial++;
        }

    }
}

