package com.zafin.zplatform.apitest.ppc.constant;

public class Endpoint {
    // API URI
//    public static final String ENDPOINT = "http://localhost:8070/";
    public static final String ENDPOINT = "http://zrpe.zrpe.svc:8080/zrpe/";
    
    public static final String AUTH_ENDPOINT = "http://localhost:8070/";
     public static final String JMS_SERVER_URL = "http-remoting://localhost:8090";
//    public static final String JMS_SERVER_URL = "http-remoting://com.zafin.zplatform.wildflymq.docker:8090";
    public static final String HEADER_AUTHORIZATION = "Authorization";
    public static final String HEADER_CONTENT_TYPE = "application/vnd.zafin+json";
    public static final String HEADER_APPLICATION_JSON = "application/json";
    public static final String ACCESS_TOKEN_URI = AUTH_ENDPOINT + "api/oauth/token?grant_type=password";
    public static final String PUBLISH_ZAR_URI = ENDPOINT + "zplatform/services/rs/zmodule/publish";
    public static final String ZAR_STATUS_URI = ENDPOINT + "zplatform/services/rs/zmodule/getStatus/";
    public static final String BRANCH_URI = ENDPOINT + "ui/services/rs/branch/";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String ZAFIN_CLIENT_CONFIG = "zafin";

    // Postgres properties

    public static final String POSTGRES_DRIVER = "org.postgresql.Driver";
    public static final String POSTGRES_URL = "jdbc:postgresql://localhost:5432/zafin";
    public static final String POSTGRES_USERNAME = "zplatform";
    public static final String POSTGRES_PASSWORD = "zplatform";
    // Oracle properties

    public static final String ORACLE_DRIVER = "oracle.jdbc.driver.OracleDriver";
    public static final String ORACLE_URL = "jdbc:oracle:thin:@localhost:1521/mirev";
    public static final String ORACLE_USERNAME = "zplatform_owner";
    public static final String ORACLE_PASSWORD = "zplatform";
    // HTTP Status Code
    public static final int OK_200 = 200;
    public static final int BAD_REQUEST_404 = 400;
    public static final int UNAUTHORIZED_401 = 401;
    public static final int NOT_FOUND_404 = 404;
    public static final int METHOD_NOT_ALLOWED_405 = 405;
    public static final int INTERNAL_SERVER_ERROR_500 = 500;

    // Workflow Actions
    public static final String SUBMIT_WORKFLOW = "/submit";
    public static final String CLAIM_WORKFLOW = "/task/claim";
    public static final String APPROVE_WORKFLOW = "/task/approve";
    public static final String MASTER_BRANCHID = "master";

    // Domain Models
    public static final String NOTIFICATION_CONFIG_URI = "/domain/models.notification.NotificationConfig/";
    public static final String REFERENCE_URI = "/services/rs/branch/";
    public static final String CUSTOMER_URI = "/domain/tel.models.Customer/";
    public static final String EVENT_CUSTOMER_EVENT = "/domain/tel.events.CustomerEvent";
    public static final String MMTEST_MODELA_URI = "/domain/mmtest.models.ModelA";
    public static final String MMTEST_MODELB_URI = "/domain/mmtest.models.ModelB";
    public static final String MMTEST_MODELE_URI = "/domain/mmtest.models.ModelE";
    public static final String MMTEST_MODELG_URI = "/domain/mmtest.models.ModelG";
    public static final String MMTEST2_MODELA_URI = "/domain/mmtest2.models.ModelA";
    public static final String MMTEST2_MODELB_URI = "/domain/mmtest2.models.ModelB";
    public static final String MMTEST2_MODELG_URI = "/domain/mmtest2.models.ModelG";
    public static final String MMTEST3_MODELA_URI = "/domain/mmtest3.models.ModelA";
    public static final String MMTEST3_MODELB_URI = "/domain/mmtest3.models.ModelB";
    public static final String BANK_PRODUCT_URI = "/domain/ppc.models.BankProduct";
    public static final String ERROR_MESSAGE = "Assertion failed! The incorrect value comes from attribute : ";

    // Thread Sleep
    public static final long INTERVAL = 2000L;
    public static final int TRIAL_MAXIMUM = 30;
    public static final long JMS_INTERVAL = 15000L;
    public static final int JMS_TRIAL_MAXIMUM = 3;

    // Zar Status
    public static final String ZAR_LOADED = "LOADED";
    public static final String ZAR_FAILED = "FAILED";

    // Branch Status
    public static final String BRANCH_DRAFT = "DRAFT";
    public static final String BRANCH_COMPLETE = "COMPLETE";
    public static final String BRANCH_UNCLAIMED = "UNCLAIMED";
    public static final String BRANCH_CLAIMED = "CLAIMED";
    public static final String BRANCH_REWORK = "REWORK";
    public static final String BRANCH_MERGING = "MERGING";
    public static final String BRANCH_SUSPENDED = "SUSPENDED";
    public static final String BRANCH_DELETED = "DELETED";

    // Workflow Status
    public static final String WORKFLOW_DRAFT = "DRAFT";
    public static final String WORKFLOW_REVIEW = "WORKFLOW_REVIEW";
    public static final String WORKFLOW_REWORK = "WORKFLOW_REWORK";
    public static final String WORKFLOW_DELETED = "WORKFLOW_DELETED";
    public static final String WORKFLOW_MERGE_QUEUED = "WORKFLOW_MERGE_QUEUED";
    public static final String WORKFLOW_COMPLETE = "WORKFLOW_COMPLETE";
    // Event Status
    public static final String EVENT_SUCCESS = "SUCCESS";
    public static final String EVENT_CLIENT_ERROR = "CLIENT_ERROR";
    // JMS Client
    public static final String JMS_DESTINATION_NAME_MMTEST = "jms/queue/mmtest";
    public static final String JMS_DESTINATION_NAME_MMTEST1 = "jms/queue/mmtest1";
    public static final String JMS_DESTINATION_NAME_MMTEST2 = "jms/queue/mmtest2";
    public static final String JMS_DESTINATION_NAME_MMTEST3 = "jms/queue/mmtest3";
    public static final String JMS_CLIENT_PORT = "http://localhost:8081/";
    public static final String ZAFIN_TEST_APP = "ZafinTestApp/";
    public static final String INITIALIZE_JMS_SERVER = JMS_CLIENT_PORT + ZAFIN_TEST_APP + "initializeJmsServer";
    public static final String REGISTER_JMS_LISTENER = JMS_CLIENT_PORT + ZAFIN_TEST_APP + "registerJMSListener";
    public static final String GET_JMS_MESSAGE = JMS_CLIENT_PORT + ZAFIN_TEST_APP + "getJmsMessage";
    public static final String CLEAR_JMS_MESSAGE = JMS_CLIENT_PORT + ZAFIN_TEST_APP + "clearJmsMessage";
    public static final String INITIAL_CONTEXT_FACTORY = "org.jboss.naming.remote.client.InitialContextFactory";
    public static final String JMS_USERNAME = "jmsuser";
    public static final String JMS_USERPASSWORD = "jmsuser@123";
    public static final String DEFAULT_CONNECTION_FACTORY = "jms/RemoteConnectionFactory";

    // XML fileNames
    public static final String MMTEST_MODELB_XML = "mmtestB.xml";
    public static final String MMTEST_MODELA_REFERENCE_MMTEST_MODELB_XML = "mmtestA-reference-mmtestB.xml";
    public static final String MMTEST_MODELA_CONTAINED_MMTEST_MODELC_XML = "mmtestA-contained-mmtestC.xml";

    // Core XSD fileNames
    public static final String CORE_XSD_VERSION = "4.1.0.xsd";
    public static final String CORE_MODELS_XSD = "core-models-" + CORE_XSD_VERSION;
    public static final String CORE_NOTIFICATION_XSD = "core-notification-" + CORE_XSD_VERSION;
    public static final String CORE_DECISIONTABLE_XSD = "core-decisiontable-" + CORE_XSD_VERSION;
    public static final String CORE_GRID_XSD = "core-grid-" + CORE_XSD_VERSION;
    public static final String CORE_UISETTING_XSD = "core-uisetting-" + CORE_XSD_VERSION;
    public static final String CORE_VERSION_XSD = "core-version-" + CORE_XSD_VERSION;
    public static final String CORE_WORKFLOW_XSD = "core-workflow-" + CORE_XSD_VERSION;
    // Impl XSD fileNames
    public static final String IMPL_XSD_VERSION = "1.0.0.xsd";
    public static final String MMTEST_MODELS_XSD = "mmtest-models-" + IMPL_XSD_VERSION;
    public static final String MMTEST_DECISIONTABLES_XSD = "mmtest-decisiontables-" + IMPL_XSD_VERSION;
    public static final String MMTEST_ENUMS_XSD = "mmtest-enums-" + IMPL_XSD_VERSION;
    public static final String MMTEST_EVENTRESULTS_XSD = "mmtest-eventresults-" + IMPL_XSD_VERSION;
    public static final String MMTEST_EVENTS_XSD = "mmtest-events-" + IMPL_XSD_VERSION;

    public static final String MMTEST1_MODELS_XSD = "mmtest1-models-" + IMPL_XSD_VERSION;
    public static final String MMTEST1_DECISIONTABLES_XSD = "mmtest1-decisiontables-" + IMPL_XSD_VERSION;
    public static final String MMTEST1_ENUMS_XSD = "mmtest1-enums-" + IMPL_XSD_VERSION;
    public static final String MMTEST1_EVENTRESULTS_XSD = "mmtest1-eventresults-" + IMPL_XSD_VERSION;
    public static final String MMTEST1_EVENTS_XSD = "mmtest1-events-" + IMPL_XSD_VERSION;

    public static final String MMTEST2_MODELS_XSD = "mmtest2-models-" + IMPL_XSD_VERSION;
    public static final String MMTEST2_DECISIONTABLES_XSD = "mmtest2-decisiontables-" + IMPL_XSD_VERSION;
    public static final String MMTEST2_ENUMS_XSD = "mmtest2-enums-" + IMPL_XSD_VERSION;
    public static final String MMTEST2_EVENTRESULTS_XSD = "mmtest2-eventresults-" + IMPL_XSD_VERSION;
    public static final String MMTEST2_EVENTS_XSD = "mmtest2-events-" + IMPL_XSD_VERSION;

    public static final String MMTEST3_MODELS_XSD = "mmtest3-models-" + IMPL_XSD_VERSION;
    public static final String MMTEST3_DECISIONTABLES_XSD = "mmtest3-decisiontables-" + IMPL_XSD_VERSION;
    public static final String MMTEST3_ENUMS_XSD = "mmtest3-enums-" + IMPL_XSD_VERSION;
    public static final String MMTEST3_EVENTRESULTS_XSD = "mmtest3-eventresults-" + IMPL_XSD_VERSION;
    public static final String MMTEST3_EVENTS_XSD = "mmtest3-events-" + IMPL_XSD_VERSION;
}
