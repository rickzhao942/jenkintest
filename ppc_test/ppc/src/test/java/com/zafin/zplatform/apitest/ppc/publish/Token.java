package com.zafin.zplatform.apitest.ppc.publish;

import static io.restassured.RestAssured.given;

import com.zafin.zplatform.apitest.ppc.constant.Endpoint;

import io.restassured.response.Response;

public class Token {

    public static Response generate(String username, String password) {
        Response response = given().auth()
                                   .basic(Endpoint.ZAFIN_CLIENT_CONFIG, Endpoint.ZAFIN_CLIENT_CONFIG)
                                   .contentType(Endpoint.HEADER_APPLICATION_JSON)
                                   .queryParam(Endpoint.USERNAME, username)
                                   .queryParam(Endpoint.PASSWORD, password)
                                   .when()
                                   .post(Endpoint.ACCESS_TOKEN_URI)
                                   .then()
                                   .log()
                                   .all()
                                   .statusCode(Endpoint.OK_200)
                                   .extract()
                                   .response();
        
        return response;
    }

}
